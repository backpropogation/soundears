package com.pokorili.musicOnAPI.controller;


import com.pokorili.musicOnAPI.entity.Genre;
import com.pokorili.musicOnAPI.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GenreController {

    @Autowired
    private GenreService service;

    @GetMapping("/genres")
    List<Genre> all() {
        return service.GetAllGenres();
    }

    @GetMapping("/genres/{id}")
    Genre GetGenre(@PathVariable("id") Long id) {
        return service.GetGenre(id);
    }

    @GetMapping(value = "/genres", params = {"title"})
    Genre getGenreByTitle(@RequestParam String title) {
        return service.GetGenre(title);
    }

    @GetMapping("/genres/{id}/children")
    List<Genre> getChildGenres(@PathVariable("id") Long id) {
        return service.GetChildGenres(service.GetGenre(id));
    }

    @PostMapping(value = "/genres", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Genre newGenre(@RequestBody Genre genre) {
        return service.AddGenre(genre);
    }

    @PostMapping("/genres/{id}")
    Genre updateGenre(@PathVariable("id") Long id, @RequestParam String title, @RequestParam(required = false) Long parentId) {
        if (parentId == null) {
            return service.UpdateGenre(id, title);
        } else {
            return service.UpdateGenre(id, title, parentId);
        }
    }


    @DeleteMapping("/genres/{id}")
    void deleteGenre(@PathVariable("id") Long id) {
        service.DeleteGenre(id);
    }
}
