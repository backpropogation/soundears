package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class VisitorController {

    @Autowired
    VisitorService service;

    @GetMapping("/visitors")
    Iterable<Visitor> GetAll() {
        return service.GetAll();
    }

    @GetMapping("/visitors/{id}")
    Visitor GetVisitor(@PathVariable Long id) {
        return service.GetVisitor(id);
    }

    @GetMapping(value = "/visitors", params = "nickname")
    Visitor GetVisitorByNickname(@RequestParam String nickname) {
        return service.GetVisitorByNickname(nickname);
    }


    @GetMapping(value = "/visitors", params = "email")
    Visitor GetVisitorByEmail(@RequestParam String email) {
        return service.GetVisitorByEmail(email);
    }

    @PostMapping(value = "/visitors/{id}", params = "status")
    Visitor ChangeVisitorStatus(@PathVariable Long id, @RequestParam String status){
        return service.ChangeStatus(id, status);
    }

    @PostMapping(value = "/visitors/{id}", params = "email")
    Visitor ChangeVisitorEmail(@PathVariable Long id, @RequestParam String email) {
        return service.ChangeEmail(id, email);
    }

    @PostMapping(value = "/visitors/{id}", params = "password")
    Visitor ChangeVisitorPassword(@PathVariable Long id, @RequestParam String password) {
        return service.ChangePassword(id, password);
    }

    @PostMapping(value = "/visitors/{id}", params = "nickname")
    Visitor ChangeVisitorNickname(@PathVariable Long id, @RequestParam String nickname) {
        return service.ChangeNickname(id, nickname);
    }

    @PostMapping(value = "/visitors", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    Visitor AddVisitor(@RequestBody Visitor visitor) {
        return service.AddVisitor(visitor);
    }

    @GetMapping("/deletevisitor/{id}")
    void deleteVisitor(@PathVariable Long id){
        service.DeleteVisitor(id);
    }
}
