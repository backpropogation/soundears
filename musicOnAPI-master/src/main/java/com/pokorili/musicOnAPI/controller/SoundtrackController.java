package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.Genre;
import com.pokorili.musicOnAPI.entity.Soundtrack;
import com.pokorili.musicOnAPI.service.SoundtrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SoundtrackController {
    @Autowired
    private SoundtrackService service;

    @GetMapping("/allsoundtracks")
    Iterable<Soundtrack> getAllSoundtracks(){
        return service.GetAllSoundtracks();
    }

    @GetMapping("/soundtrack/{id}")
    Soundtrack getSoundtrack(@PathVariable Long id){
        return  service.GetSoundtrack(id);
    }


    @GetMapping(value = "/soundtrack", params = {"title"})
    Iterable<Soundtrack> getSoundtrackByTitle(@RequestParam String title) {
        return service.GetSoundtracksByTitle(title);
    }

    @GetMapping(value = "/soundtrack", params = {"author"})
    Iterable<Soundtrack> getSoundtrackByAuthor(@RequestParam String author){
        return service.GetSoundtracksByAuthor(author);
    }


    @GetMapping("/soundtracks/{id}")
    Iterable<Soundtrack> getSoundtrackByVisitor(@PathVariable Long id) {
        return service.GetSoundtracksByVisitorId(id);
    }

    @GetMapping(value = "/soundtracks/{id}", params = {"title"})
    Iterable<Soundtrack> getSoundtrackByVisitorAndTitle(@PathVariable Long id, @RequestParam String title) {
        return service.GetSoundtracksByVisitorIdAndTitle(id, title);
    }

    @GetMapping(value = "/soundtracks", params = {"genreId"})
    Iterable<Soundtrack> getSoundtrackByGenre(@RequestParam Long genreId){
        return service.GetSoundtracksByGenre(genreId);
    }

    @PostMapping(value = "/addsoundtrack", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Soundtrack addSoundtrack(@RequestBody Soundtrack soundtrack){
        return service.AddSoundtrack(soundtrack);
    }

    @GetMapping(value = "/deletesoundtrack/{id}")
    void deleteSoundtrack(@PathVariable Long id){
        service.DeleteSoundtrack(id);
    }

}
