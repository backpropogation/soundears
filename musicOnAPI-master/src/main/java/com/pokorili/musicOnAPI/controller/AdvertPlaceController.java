package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.AdvertPlace;
import com.pokorili.musicOnAPI.service.AdvertPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class AdvertPlaceController {

    @Autowired
    AdvertPlaceService service;

    @GetMapping("/advertPlaces/{id}")
    AdvertPlace GetPlace(@PathVariable Long id) {
        return service.GetPlace(id);
    }

    @GetMapping("/advertPlaces")
    Iterable<AdvertPlace> GetPlaces() {
        return service.GetPlaces();
    }

    @GetMapping(value = "/advertPlaces", params = {"status"})
    Iterable<AdvertPlace> GetFreePlaces(@RequestParam String status) {
        return service.GetPlaces(status);
    }

    @PostMapping(value = "/advertPlaces", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AdvertPlace AddPlace(@RequestBody AdvertPlace advertPlace) {
        return service.AddAdvertPlace(advertPlace);
    }

    @PostMapping(value = "/advertPlaces/{id}", params = "isFree")
    AdvertPlace ChangeAvailability(@PathVariable Long id, @RequestParam Boolean isFree) {
        return service.ChangeAvailability(id, isFree);
    }

    @GetMapping("/freeplaces")
    Iterable<AdvertPlace> getFreePlaces() {
        return service.GetFreePlaces();
    }

    @GetMapping("/deleteadvertplace/{id}")
    void deleteAdvertPlace(@PathVariable Long id){
        service.DeleteAdvertPlace(id);
    }
}
