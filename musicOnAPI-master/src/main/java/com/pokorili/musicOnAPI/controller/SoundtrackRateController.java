package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.Soundtrack;
import com.pokorili.musicOnAPI.entity.SoundtrackRate;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.repository.SoundtrackRateRepository;
import com.pokorili.musicOnAPI.service.SoundtrackRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class SoundtrackRateController {

    @Autowired
    SoundtrackRateService service;

    @GetMapping("/allsoundtrackrates")
    Iterable<SoundtrackRate> getAllSoundtrackRate(){
        return service.GetAllSoundtrackRateObjs();
    }

    @GetMapping("/soundtrackrate/{id}")
    SoundtrackRate getSoundtrackRate(@PathVariable Long id){
        return  service.GetSoundtrackRateObj(id);
    }

    @PostMapping(value = "/soundtrackrates", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Iterable<SoundtrackRate> getSoundTrackRates(@RequestBody Soundtrack soundtrack){
        return  service.GetSoundtrackRates(soundtrack);
    }

    @PostMapping(value = "/likedsoundtracks", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Iterable<Soundtrack> getLiked(@RequestBody Visitor visitor){
        return service.GetLikedSoundtracks(visitor);
    }

    @CrossOrigin
    @PostMapping("/ratesoundtrack/{soundtrackId}/{userId}/{rate}")
    Integer PostRateOnSoundtrack(@PathVariable Long soundtrackId, @PathVariable Long userId, @PathVariable Integer rate) {
        return service.PostRateOnSoundtrack(soundtrackId, userId, rate).getRate();
    }

    @CrossOrigin
    @GetMapping("/getsoundtrackrate/{soundtrackId}/{userId}")
    Integer GetSoundtrackRate(@PathVariable Long soundtrackId, @PathVariable Long userId) {
        return service.GetUserRate(soundtrackId, userId);
    }


    @GetMapping("/top10songs")
    Iterable<Soundtrack> getTop10(){
        return  service.getTopSongs();
    }

}
