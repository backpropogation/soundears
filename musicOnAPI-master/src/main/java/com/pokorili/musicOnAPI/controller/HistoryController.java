package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.History;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class HistoryController {

    @Autowired
    HistoryService service;

    @GetMapping("/allhistories")
    Iterable<History> getAllHistories(){
        return service.GetAllHistories();
    }

    @GetMapping("/gethistory/{id}")
    History getHistory(@PathVariable Long id){
        return service.GetHistory(id);
    }

    @PostMapping(value = "/gethistory", consumes =   MediaType.APPLICATION_JSON_UTF8_VALUE)
    Iterable<History> getVisitorsHistory(@RequestBody Visitor visitor){
        return service.GetVisitorsHistory(visitor);
    }

    @PostMapping(value = "/addhistory", consumes =   MediaType.APPLICATION_JSON_UTF8_VALUE)
    History addHistory (@RequestBody History history){
        return service.AddHistory(history);
    }

    @PostMapping(value = "/deletehistory", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void deleteHistory(@RequestBody History history){
        service.DeleteHistory(history);
    }
}
