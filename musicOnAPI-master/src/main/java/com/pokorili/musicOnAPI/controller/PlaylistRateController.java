package com.pokorili.musicOnAPI.controller;


import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.PlaylistRate;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.service.PlaylistRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;

@RestController
public class PlaylistRateController {
    @Autowired
    PlaylistRateService service;

    @GetMapping("/allplaylistrateobjs")
    Iterable<PlaylistRate> getAllPlaylistRateobjs(){
        return service.GetAllPlaylistRateObjs();
    }

    @GetMapping("/playlistrateobj/{id}")
    PlaylistRate getPlaylistRate(@PathVariable Long id){
        return  service.GetPlaylistRateObj(id);
    }

    @PostMapping(value = "/playlistrates", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Iterable<PlaylistRate> getPlaylistRates(@RequestBody Playlist playlist){
        return service.GetPlaylistRates(playlist);
    }

    @PostMapping(value = "/likedplaylists", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Iterable<Playlist> getLikedPlaylists(@RequestBody Visitor visitor){
        return service.GetLikedPlaylists(visitor);
    }

    @CrossOrigin
    @GetMapping("/getplaylistrate/{playlistId}/{visitorId}")
    Integer getPlaylistRate(@PathVariable Long playlistId, @PathVariable Long visitorId) {
        PlaylistRate playlistRate = service.GetPlaylistRate(playlistId, visitorId);
        if(playlistRate == null) {
            return 0;
        } else {
            return playlistRate.getRate();
        }
    }

    @CrossOrigin
    @PostMapping("/rateplaylist/{playlistId}/{visitorId}/{rate}")
    Integer ratePlaylist(@PathVariable Long playlistId, @PathVariable Long visitorId, @PathVariable Integer rate) {
        return service.AddRateOnPlayList(playlistId, visitorId, rate).getRate();
    }

    @GetMapping("/top10playlists")
    Iterable<Playlist> getTop10(){
        return  service.getTopPlaylists();
    }
}
