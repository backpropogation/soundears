package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.Advert;
import com.pokorili.musicOnAPI.entity.AdvertPlace;
import com.pokorili.musicOnAPI.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class AdvertController {
    @Autowired
    AdvertService service;

    @GetMapping("/alladverts")
    Iterable<Advert> getAllAdverts(){
        return service.GetAllAdverts();
    }

    @GetMapping("/advert/{id}")
    Advert getAdvert(@PathVariable Long id){
        return service.GetAdvert(id);
    }

    @GetMapping("/activeadverts")
    Iterable<Advert> getActiveAdverts(){
        return service.GetActiveAdverts();
    }

    @CrossOrigin
    @GetMapping(value = "/getadvertbyplace/{id}")
    Advert getAdvertByPlaceId(@PathVariable Long id){
        return  service.GetAdvertByPlaceId(id);
    }

    @PostMapping(value = "/deleteadvert", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void deleteAdvert(@RequestBody Advert advert){
        service.DeleteAdvert(advert);
    }

    @GetMapping("/deactivateadvert/{id}")
    void deactivateAdvert(@PathVariable Long id) {
        service.DeactivateAdvert(id);
    }

    @GetMapping("/getnewadverts")
    Iterable<Advert> getNewAdverts(){
        return service.GetNewAdverts();
    }

    @GetMapping("/getapprovedadverts")
    Iterable<Advert> getApprovedAdverts(){
        return service.GetApprovedAdverts();
    }

    @PostMapping(value = "/addadvert", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Advert addAdvert(@RequestBody Advert advert) {
        return service.AddAdvert(advert);
    }

    @PostMapping(value = "/changeapproval", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Advert changeApprovalStatus(@RequestBody Advert advert, @RequestParam Boolean approval){
        return service.ChangeApprovalStatus(advert, approval);
    }
}
