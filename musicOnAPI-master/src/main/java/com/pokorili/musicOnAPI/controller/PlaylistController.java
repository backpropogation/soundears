package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;

@RestController
public class PlaylistController {

    @Autowired
    private PlaylistService service;

    @GetMapping("/allplaylists")
    Iterable<Playlist> getAllPlaylists(){
        return service.GetAllPlaylists();
    }

    @GetMapping("/playlist/{id}")
    Playlist getPlaylist(@PathVariable Long id){
        return service.GetPlaylist(id);
    }

    @GetMapping(value = "/playlists", params = {"title"})
    Iterable<Playlist> getPlaylistByTitle(@RequestParam String title){
        return service.GetPlaylists(title);
    }

    @CrossOrigin
    @GetMapping(value = "/playlists/{id}")
    Iterable<Playlist> getPlaylistByVisitor(@PathVariable Long id){
        return service.GetPlaylists(id);
    }

    @GetMapping(value = "/playlists/{id}", params = {"title"})
    Iterable<Playlist> getPlaylistByVisitorAndTitle(@PathVariable Long id, @RequestParam String title){
        return service.GetPlaylistsByTitle(id, title);
    }

    @PostMapping(value = "/playlist/{id}", params = {"title", "description"})
    Playlist changePlaylistParams(@PathVariable Long id, @RequestParam String title, @RequestParam String description) {
        return service.changePlaylistParams(id, title, description);
    }

    @PostMapping(value = "/addplaylist", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Playlist newPlaylist(@RequestBody Playlist playlist){
        return service.AddPlaylist(playlist);
    }

    @GetMapping("/deleteplaylist/{id}")
    void deletePlaylist(@PathVariable Long id){
        service.DeletePlaylist(id);
    }

}
