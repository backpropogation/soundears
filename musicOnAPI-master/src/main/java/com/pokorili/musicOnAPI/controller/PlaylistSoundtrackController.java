package com.pokorili.musicOnAPI.controller;

import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.PlaylistSoundtrack;
import com.pokorili.musicOnAPI.entity.Soundtrack;
import com.pokorili.musicOnAPI.service.PlaylistSoundtrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PlaylistSoundtrackController {

    @Autowired
    private PlaylistSoundtrackService service;

    @GetMapping("/allplaylistsoundtrack")
    Iterable<PlaylistSoundtrack> getAllPlaylistSoundtrackObj(){
        return service.GetAllPlaylistSoundtrackObjs();
    }

    @GetMapping("/playlistsoundtrackobj/{id}")
    PlaylistSoundtrack getPlaylistSoundtrack(@PathVariable Long id){
        return  service.GetPlaylistSoundtrackObj(id);
    }

    @CrossOrigin
    @GetMapping("/playlistsoundtrack/{playlistId}/{soundtrackId}")
    PlaylistSoundtrack getPlaylistSoundtrackObj(@PathVariable Long playlistId, @PathVariable Long soundtrackId){
        return  service.GetPlaylistSoundtrackObj(playlistId, soundtrackId);
    }

    @GetMapping("/soundtrackonplaylist/{id}")
    Iterable<Soundtrack> getSoundtrackOnPlaylist(@PathVariable Long id){
        return  service.GetSoundtrackOnPlaylistById(id);
    }

    @CrossOrigin
    @PostMapping("/addplaylistsoundtrack/{playlistId}/{soundtrackId}")
    PlaylistSoundtrack addPlaylistSoundtrackObj(@PathVariable Long playlistId, @PathVariable Long soundtrackId){
        return service.AddPlaylistSoundtrackObj(playlistId, soundtrackId);
    }

    @PostMapping("/addplaylistsoundtrackobj")
    PlaylistSoundtrack addPlaylistSoundtrackObj(@RequestBody PlaylistSoundtrack playlistSoundtrack){
        return service.AddPlaylistSoundtrackObj(playlistSoundtrack);
    }

    @GetMapping("/deleteplaylistsoundtrack/{playlistId}/{soundtrackId}")
    void deletePlaylistSoundtrackObj(@PathVariable Long playlistId, @PathVariable Long soundtrackId){
        service.DeletePlaylistSoundtrackObj(playlistId, soundtrackId);
    }
}
