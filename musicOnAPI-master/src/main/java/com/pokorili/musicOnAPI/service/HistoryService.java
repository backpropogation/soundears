package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.History;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryService {

    @Autowired
    HistoryRepository repository;

    public Iterable<History> GetAllHistories() {
        return repository.findAll();
    }

    public History GetHistory(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<History> GetVisitorsHistory(Visitor visitor) {
        return repository.findAllByVisitor(visitor);
    }

    public History AddHistory(History history) {
        return repository.save(history);
    }

    public void DeleteHistory(History history) {
        repository.delete(history);
    }
}
