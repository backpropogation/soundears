package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.VisitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisitorService {

    @Autowired
    VisitorRepository repository;

    public Visitor GetVisitor(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Visitor GetVisitorByNickname(String nickname) {
        return  repository.findByNickname(nickname).orElseThrow(ResourceNotFoundException::new);
    }
    public Visitor GetVisitorByEmail(String email) {
        return  repository.findByEmail(email).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<Visitor> GetAll() {
        return repository.findAll();
    }

    public Visitor ChangeStatus(Long id, String status) {

        Visitor visitor = GetVisitor(id);
        visitor.setStatus(status);
        return repository.save(visitor);
    }

    public Visitor ChangeEmail(Long id, String email) {
        Visitor visitor = GetVisitor(id);
        visitor.setEmail(email);
        return repository.save(visitor);
    }

    public Visitor ChangePassword(Long id, String password) {
        Visitor visitor = GetVisitor(id);
        visitor.setPassword(password);
        return repository.save(visitor);
    }

    public Visitor ChangeNickname(Long id, String nickname) {
        Visitor visitor = GetVisitor(id);
        visitor.setNickname(nickname);
        return repository.save(visitor);
    }

    public Visitor AddVisitor(Visitor visitor) {
        return repository.save(visitor);
    }

    public void DeleteVisitor(Long id) {
        repository.deleteById(id);
    }
}
