package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.PlaylistRate;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.PlaylistRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;

@Service
public class PlaylistRateService {

    @Autowired
    PlaylistRateRepository repository;

    @Autowired
    PlaylistService playlistService;

    @Autowired
    VisitorService visitorService;

    public Iterable<PlaylistRate> GetAllPlaylistRateObjs() {
        return repository.findAll();
    }

    public PlaylistRate GetPlaylistRateObj(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<PlaylistRate> GetPlaylistRates(Playlist playlist) {
        return repository.findAllByPlaylist(playlist);
    }

    private Iterable<PlaylistRate> GetLikedPlaylistRateObjs(Visitor visitor) {
        return repository.findAllByVisitorAndRate(visitor, 1);
    }

    public Iterable<Playlist> GetLikedPlaylists(Visitor visitor) {
        Iterable<PlaylistRate> rates = GetLikedPlaylistRateObjs(visitor);
        ArrayList<Playlist> playlists = new ArrayList<Playlist>();
        for (PlaylistRate playlistRate : rates) {
            playlists.add(playlistRate.getPlaylist());
        }
        return playlists;
    }

    public PlaylistRate GetPlaylistRate(Long playlistId, Long visitorId) {
        Playlist playlist = playlistService.GetPlaylist(playlistId);
        Visitor visitor = visitorService.GetVisitor(visitorId);
        return repository.findByPlaylistAndVisitor(playlist, visitor).orElse(null);
    }

    public PlaylistRate AddRateOnPlayList(Long playlistId, Long visitorId, Integer rate) {
        PlaylistRate playlistRate = GetPlaylistRate(playlistId, visitorId);
        if(playlistRate == null) {
            Playlist playlist = playlistService.GetPlaylist(playlistId);
            Visitor visitor = visitorService.GetVisitor(visitorId);
            return repository.save(new PlaylistRate(playlist, visitor, rate));
        } else {
            playlistRate.setRate(rate);
            return repository.save(playlistRate);
        }
    }

    public ArrayList<Playlist> getTopPlaylists(){
        Iterable<BigInteger> asd = repository.findTop10Playlists();
        ArrayList<Playlist> ret = new ArrayList<>();
        for (BigInteger a : asd){
            ret.add(playlistService.GetPlaylist(a.longValue()));
        }
        return ret;
    }
}
