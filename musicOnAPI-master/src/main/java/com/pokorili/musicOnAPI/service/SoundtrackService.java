package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Genre;
import com.pokorili.musicOnAPI.entity.Soundtrack;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.SoundtrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SoundtrackService {

    @Autowired
    SoundtrackRepository repository;

    @Autowired
    GenreService genreService;

    public Iterable<Soundtrack> GetAllSoundtracks() {
        return repository.findAll();
    }

    public Soundtrack GetSoundtrack(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<Soundtrack> GetSoundtracksByVisitorId(Long id) {
        return repository.findAllByVisitorId(id);
    }

    public Iterable<Soundtrack> GetSoundtracksByVisitorIdAndTitle(Long id, String title) {
        return repository.findAllByVisitorIdAndTitleIgnoreCase(id, title);
    }

    public Iterable<Soundtrack> GetSoundtracksByTitle(String title) {
        return repository.findByTitleIgnoreCase(title);
    }

    public Iterable<Soundtrack> GetSoundtracksByAuthor(String author) {
        return repository.findByAuthorIgnoreCase(author);
    }

    public Iterable<Soundtrack> GetSoundtracksByGenre(Long genreId) {
        Genre genre = genreService.GetGenre(genreId);
        return repository.findAllByGenre(genre);
    }

    public Soundtrack AddSoundtrack(Soundtrack soundtrack) {
        return repository.save(soundtrack);
    }

    public void DeleteSoundtrack(Long id) {
        repository.deleteById(id);
    }

}
