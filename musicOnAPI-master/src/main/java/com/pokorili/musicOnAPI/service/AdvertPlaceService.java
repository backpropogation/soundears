package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.AdvertPlace;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.AdvertPlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdvertPlaceService {

    @Autowired
    AdvertPlaceRepository repository;

    public AdvertPlace GetPlace(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<AdvertPlace> GetPlaces() {
        return repository.findAll();
    }

    public Iterable<AdvertPlace> GetPlaces(String status) {
        if (status.equals("free")) {
            return repository.findByIsFreeIs(true);
        } else {
            return repository.findByIsFreeIs(false);
        }
    }

    public Iterable<AdvertPlace> GetFreePlaces() {
        return repository.findByIsFreeIs(true);
    }

    public  AdvertPlace AddAdvertPlace(AdvertPlace advertPlace) {
        return repository.save(advertPlace);
    }

    public void DeleteAdvertPlace(Long id) {
        repository.deleteById(id);
    }

    public AdvertPlace ChangeAvailability(Long id, Boolean isFree) {
        AdvertPlace advertPlace = GetPlace(id);
        advertPlace.setIsFree(isFree);
        return repository.save(advertPlace);
    }

}
