package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Soundtrack;
import com.pokorili.musicOnAPI.entity.SoundtrackRate;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.SoundtrackRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;

@Service
public class SoundtrackRateService {

    @Autowired
    SoundtrackRateRepository repository;

    @Autowired
    SoundtrackService soundtrackService;

    @Autowired
    VisitorService visitorService;



    public Iterable<SoundtrackRate> GetAllSoundtrackRateObjs() {
        return repository.findAll();
    }

    public SoundtrackRate GetSoundtrackRateObj(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public SoundtrackRate GetSoundtrackRateObj(Long soundtrackId, Long visitorId) {
        Soundtrack soundtrack = soundtrackService.GetSoundtrack(soundtrackId);
        Visitor visitor = visitorService.GetVisitor(visitorId);
        return repository.findBySoundtrackAndVisitor(soundtrack, visitor).orElse(null);
    }

    public Iterable<SoundtrackRate> GetSoundtrackRates(Soundtrack soundtrack) {
        return repository.findAllBySoundtrack(soundtrack);
    }

    private Iterable<SoundtrackRate> GetLikedSoundtrackRateObjs(Visitor visitor) {
        return repository.findAllByVisitorAndRate(visitor, 1);
    }

    public Iterable<Soundtrack> GetLikedSoundtracks(Visitor visitor) {
        Iterable<SoundtrackRate> rates = GetLikedSoundtrackRateObjs(visitor);
        ArrayList<Soundtrack> soundtracks = new ArrayList<Soundtrack>();
        for (SoundtrackRate soundtrackRate : rates) {
            soundtracks.add(soundtrackRate.getSoundtrack());
        }
        return soundtracks;
    }

    public SoundtrackRate PostRateOnSoundtrack(Long soundtrackId, Long visitorId, Integer rate) {
        SoundtrackRate soundtrackRate = GetSoundtrackRateObj(soundtrackId, visitorId);
        if(soundtrackRate == null) {
            Soundtrack soundtrack = soundtrackService.GetSoundtrack(soundtrackId);
            Visitor visitor = visitorService.GetVisitor(visitorId);
            return repository.save(new SoundtrackRate(soundtrack, visitor, rate));
        } else {
            soundtrackRate.setRate(rate);
            return repository.save(soundtrackRate);
        }
    }

    public Integer GetUserRate(Long soundtrackId, Long visitorId) {
        SoundtrackRate soundtrackRate = GetSoundtrackRateObj(soundtrackId, visitorId);
        if (soundtrackRate == null) {
            return 0;
        } else {
            return soundtrackRate.getRate();
        }
    }


    public ArrayList<Soundtrack> getTopSongs(){
        Iterable<BigInteger> asd = repository.findTop10SongIds();
        ArrayList<Soundtrack> ret = new ArrayList<>();
        for (BigInteger a : asd){
            ret.add(soundtrackService.GetSoundtrack(a.longValue()));
        }
        return ret;
    }
}
