package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.PlaylistSoundtrack;
import com.pokorili.musicOnAPI.entity.Soundtrack;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.PlaylistSoundtrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PlaylistSoundtrackService {

    @Autowired
    PlaylistSoundtrackRepository repository;

    @Autowired
    PlaylistService playlistService;

    @Autowired
    SoundtrackService soundtrackService;

    public Iterable<PlaylistSoundtrack> GetAllPlaylistSoundtrackObjs() {
        return repository.findAll();
    }

    public PlaylistSoundtrack GetPlaylistSoundtrackObj(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    private Iterable<PlaylistSoundtrack> GetPlaylistSoundtrackObjs(Long playlist_id) {

        return repository.findAllByPlaylistId(playlist_id);
    }

    public Iterable<Soundtrack> GetSoundtrackOnPlaylistById(Long playlistId) {
        Iterable<PlaylistSoundtrack> playlistSoundtracks = GetPlaylistSoundtrackObjs(playlistId);
        ArrayList<Soundtrack> soundtracks = new ArrayList<Soundtrack>();
        for(PlaylistSoundtrack ps:playlistSoundtracks) {
            soundtracks.add(ps.getSoundtrack());
        }
        return soundtracks;
    }

    public PlaylistSoundtrack AddPlaylistSoundtrackObj(PlaylistSoundtrack playlistSoundtrack) {
        return repository.save(playlistSoundtrack);
    }

    public PlaylistSoundtrack AddPlaylistSoundtrackObj(Long playlistId, Long soundtrackId) {
        Playlist playlist = playlistService.GetPlaylist(playlistId);
        Soundtrack soundtrack = soundtrackService.GetSoundtrack(soundtrackId);
        return repository.save(new PlaylistSoundtrack(playlist, soundtrack));
    }

    public void DeletePlaylistSoundtrackObj(Long playListId, Long soundtrackId) {
        PlaylistSoundtrack playlistSoundtrack = GetPlaylistSoundtrackObj(playListId, soundtrackId);
        repository.deleteById(playlistSoundtrack.getId());
    }

    public PlaylistSoundtrack GetPlaylistSoundtrackObj(Long playlistId, Long soundtrackId) {
        Playlist playlist = playlistService.GetPlaylist(playlistId);
        Soundtrack soundtrack = soundtrackService.GetSoundtrack(soundtrackId);
        return repository.findByPlaylistAndSoundtrack(playlist, soundtrack).orElse(null);
    }
}
