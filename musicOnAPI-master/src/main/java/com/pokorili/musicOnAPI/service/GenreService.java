package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Genre;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService {
    @Autowired
    GenreRepository repository;

    public List<Genre> GetAllGenres(){
        return (List<Genre>) repository.findAll();
    }

    public Genre GetGenre(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Genre GetGenre(String title) {
        return repository.findByTitle(title).orElseThrow(ResourceNotFoundException::new);
    }

    public List<Genre> GetChildGenres(Genre genre) {
        return repository.findByParentGenre(genre);
    }

    public Genre AddGenre(String title, Long parentId) {
        return repository.save( new Genre(title, repository.findById(parentId).get()));
    }

    public Genre AddGenre(String title) {
        return repository.save(new Genre(title));
    }

    public Genre AddGenre(Genre genre) {
        return repository.save(genre);
    }

    public Genre UpdateGenre(Long id, String title, Long parentId) {
        Genre genre = repository.findById(id).orElse(new Genre(id, title, repository.findById(parentId).get()));
        genre.setTitle(title);
        genre.setParentGenre(repository.findById(parentId).get());
        return repository.save(genre);
    }

    public Genre UpdateGenre(Long id, String title) {
        Genre genre = repository.findById(id).orElse(new Genre(id, title));
        genre.setTitle(title);
        return repository.save(genre);
    }

    public void DeleteGenre(Long id) {
        repository.deleteById(id);
    }
}
