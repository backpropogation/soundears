package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Advert;
import com.pokorili.musicOnAPI.entity.AdvertPlace;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.AdvertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdvertService {

    @Autowired
    AdvertRepository repository;

    @Autowired
    AdvertPlaceService advertPlaceService;

    public Iterable<Advert> GetAllAdverts() {
        return repository.findAll();
    }

    public Advert GetAdvert(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<Advert> GetActiveAdverts() {
        return repository.findAllByAdvertPlaceNotNull();
    }

    public Advert GetAdvertByPlaceId(Long id) {
        AdvertPlace advertPlace = advertPlaceService.GetPlace(id);
        return repository.findByAdvertPlace(advertPlace).orElse(null);
    }

    public Advert AddAdvert(Advert advert) {
        System.out.println(advert.getAdvLink());
        return repository.save(advert);
    }

    public void DeactivateAdvert(Long id) {
        Advert advert = GetAdvert(id);
        advert.setAdvertPlace(null);
        repository.save(advert);
    }

    public void DeleteAdvert(Advert advert) {
        repository.delete(advert);
    }

    public Iterable<Advert> GetNewAdverts() {
        return repository.findAllByApprovalIsNullOrderByOfferDateDesc();
    }

    public Iterable<Advert> GetApprovedAdverts() {
        return repository.findAllByApproval(true);
    }

    public Advert ChangeApprovalStatus(Advert advert, Boolean approval) {
        advert.setApproval(approval);
        return AddAdvert(advert);
    }
}
