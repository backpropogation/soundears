package com.pokorili.musicOnAPI.service;

import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.Visitor;
import com.pokorili.musicOnAPI.exceptions.ResourceNotFoundException;
import com.pokorili.musicOnAPI.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaylistService {

    @Autowired
    PlaylistRepository repository;

    @Autowired
    VisitorService visitorService;

    public Iterable<Playlist> GetAllPlaylists() {
        return repository.findAll();
    }

    public Playlist GetPlaylist(Long id) {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Iterable<Playlist> GetPlaylists(String title) {
        return repository.findAllByTitleIgnoreCase(title);
    }

    public Iterable<Playlist> GetPlaylists(Long id) {
        Visitor visitor = visitorService.GetVisitor(id);
        return repository.findAllByVisitor(visitor);
    }

    public Iterable<Playlist> GetPlaylistsByTitle(Long id, String title) {
        Visitor visitor = visitorService.GetVisitor(id);
        return repository.findAllByVisitorAndTitleIgnoreCase(visitor, title);
    }

    public Playlist changePlaylistParams(Long id, String title, String description) {
        Playlist playlist = GetPlaylist(id);
        playlist.setDescription(description);
        playlist.setTitle(title);
        return repository.save(playlist);
    }

    public Playlist AddPlaylist(Playlist playlist) {
        return repository.save(playlist);
    }

    public void DeletePlaylist(Long id) {
        repository.deleteById(id);
    }
}
