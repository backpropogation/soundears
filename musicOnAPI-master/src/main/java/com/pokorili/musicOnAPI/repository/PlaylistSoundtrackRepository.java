package com.pokorili.musicOnAPI.repository;

import com.pokorili.musicOnAPI.entity.Playlist;
import com.pokorili.musicOnAPI.entity.PlaylistSoundtrack;
import com.pokorili.musicOnAPI.entity.Soundtrack;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlaylistSoundtrackRepository extends CrudRepository<PlaylistSoundtrack, Long> {
    Iterable<PlaylistSoundtrack> findAllByPlaylistId(Long id);
    Optional<PlaylistSoundtrack> findByPlaylistAndSoundtrack(Playlist playlist, Soundtrack soundtrack);
}
