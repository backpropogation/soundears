package com.pokorili.musicOnAPI.repository;

import com.pokorili.musicOnAPI.entity.History;
import com.pokorili.musicOnAPI.entity.Visitor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRepository extends CrudRepository<History,Long> {
    Iterable<History> findAllByVisitor(Visitor visitor);
}
