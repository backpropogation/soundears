package com.pokorili.musicOnAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicOnApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusicOnApplication.class, args);
	}

}

