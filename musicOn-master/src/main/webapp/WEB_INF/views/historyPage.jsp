<%@page contentType="text/html;charset=UTF-8"%>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
</head>

<body>
<%@include file="header.jsp"%>
<style>
    .favourites-tab {
        padding: 10px;
        border: 1px solid black;
        display: inline-block;
    }
</style>
<div>
    <div class="favourites-navigation">
        <div class="favourites-tab">
            <a href="/favouriteSoundtracks">Favourite Soundtracks</a>
        </div>
        <div class="favourites-tab">
            <a href="/mysoundtracks">My Soundtracks</a>
        </div>
        <div class="favourites-tab">
            <a href="/favouritePlaylists">Favourite Playlists</a>
        </div>
        <div class="favourites-tab">
            <a href="/myplaylists">My Playlists</a>
        </div>
        <div class="favourites-tab">
            <a href="/history">History</a>
        </div>
    </div>
</div>
</body>

</html>