<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        .multipleFiles{
            width: 500px;
            margin: 201px auto;
            font-size: 50px;
            text-align: center;
        }
        input[type='file']{
            margin: 30px auto;
        }
        input#upload{
            font-size: 40px;
        }

    </style>
</head>

<body>
<%@include file="header.jsp" %>

<div class="multipleFiles">
    <form:form method="POST" enctype="multipart/form-data" action="/addsongs">
        <h3>Выберите файлы: </h3>
        <input type="file" name="files" multiple accept="audio/*"
               onchange="" required/><br>
        <input id="upload" type="submit" value="Загрузить"/>
    </form:form>
</div>
</body>

</html>