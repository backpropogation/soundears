<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        .login-form {
            width: 500px;
            margin: 100px auto;
            border: 2px solid darkgray;
            border-radius: 5px;
        }

        .inputField {
            margin: 20px;
        }

        .red {
            color: red;
        }

        p.label {
            text-align: left;
            font-size: 30px;

        }

        .inputText {
            margin-top: 10px;
            font-size: 30px;
            height: 30px;
        }

        .inputField > #reset {
            float: right;
            color: navy;
            text-decoration: underline;
        }

        #login-button {
            padding: 15px;
            float: left;
            width: 150px;
            background-color: white;
            border: 1px darkgray solid;
            border-radius: 5px;
        }

        div .red {
            border: 0px solid red;
            border-radius: 5px;
            background-color: #ff9b90;
        }

        <%
        if (request.getAttribute("errMessage") != null) {
        %>
        div .red p {
            padding: 5px;
        }

        <%
        }
        %>
    </style>
</head>

<body>
<%@include file="header.jsp" %>
<div class="inputField">
    <p class="">${infoMessage}</p>
</div>
<div class="login-form" align="center">
    <%--@elvariable id="user" type="com.pokorili.musicOn.entity.Visitor"--%>
    <form:form method="post" action="/login" modelAttribute="user">
        <div class="inputField">
            <label>
                <p class="label">Логин:</p>
                <form:input type="text" path="nickname" cssClass="inputText"/>
            </label>
        </div>
        <div class="inputField">
            <label>
                <p class="label">Пароль:</p>
                <form:input type="password" path="password" cssClass="inputText"/>
            </label>
        </div>
        <div class="inputField red">
            <p class="red">${errMessage}</p>
        </div>
        <div style="height: 60px">
            <div class="inputField">
                <a href="/resetPassword" id="reset">Сбросить пароль</a>
            </div>
            <div></div>
            <div class="inputField">
                <input type="submit" value="Войти" id="login-button">
            </div>
        </div>
    </form:form>
</div>
</body>
<script>
</script>
</html>