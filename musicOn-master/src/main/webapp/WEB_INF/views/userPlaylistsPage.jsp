<%--@elvariable id="title" type="java.lang.String"--%>
<%--@elvariable id="soundtrackTitle" type="java.lang.String"--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        a:visited {
            color: black;
        }

        a {
            color: black;
            text-decoration: none;
        }

        .searchType {
            margin-top: 20px;
            margin-left: 10px;
            width: fit-content;
            padding: 2px;
            border: 2px solid black;
            padding: 3px;
        }

        .searchChoose {
            display: inline-block;
            float: left;
        }

        .container {
            display: inline-block;
            margin: 0 100px;
        }

        .forminput {
            margin-top: 20px;
        }

        .searchResults {
            margin-top: 20px;
        }

        .songs {
            padding: 5px;
            margin-top: 4px;
            border: 1px solid black;
        }

        .playlists {
            padding: 5px;
            margin-top: 4px;
            border: 1px solid black;
            width: 400px;
        }

        .favourites-tab {
            padding: 10px;
            border: 1px solid black;
            display: inline-block;
        }

        .active {
            color: red;
        }

        .passive {
            color: darkblue;
        }

        * {
            padding: 0;
            margin: 0;
        }

        a:visited {
            color: black;
        }

        a {
            color: black;
            text-decoration: none;
        }

        .searchType {
            margin: 10px;
            width: fit-content;
            border: 2px solid black;
            padding: 3px;
            display: inline-block;
        }

        .searchChoose {
            text-align: center;
        }

        .container {
            margin: 0 100px;
        }

        .forminput {
            margin-top: 20px;
        }

        .searchResults {
            margin-top: 20px;
        }

        .songs {
            padding: 5px;
            margin-top: 10px;
            border: 0px;
            width: 600px;
            height: 65px;
            background-color: #f1f3f4;
            border-radius: 20px;
        }

        .playlists {
            padding: 5px;
            margin-top: 4px;
            border: 0px;
            width: 600px;
            height: 40px;
            border-radius: 20px;
            background-color: #f1f3f4;
        }

        .playlists > * {
            margin-top: 10px;
        }

        .passive {
            color: darkblue;
        }

        .active {
            background-color: red;

        }

        .leftSong {
            display: inline-block;
        }

        .rightSong {
            display: inline-block;
            vertical-align: top;
            margin-top: 2px;
        }

        .titleAndAuthor {
            width: 300px;
            font-size: 20px;
            text-align: center;
            margin-bottom: 5px;
        }

        audio {
            width: 350px;
            height: 30px;
        }

        button:active {
            border: none;
        }

        .rightSongButtons > * {
            margin-left: 20px;
            height: 20px;

        }

        .addToPlaylist {
            align-content: center;
            text-align: center;
            margin-top: 10px;
        }

        .leftPlaylist {
            display: inline-block;
            vertical-align: top;
            width: 400px;
        }

        .rightPlaylist {
            display: inline-block;
            margin-left: 5px;
        }

        .leftPlaylist a {
            margin-left: 90px;
            font-weight: bold;
            font-size: 25px;
        }

        .rate {
            border: none;
        }

        button.dislike-button.active {
            background-color: black;
        }


    </style>
</head>

<body>
<%@include file="header.jsp" %>
<div class="mainContent">

    <div class="favourites-navigation">
        <div class="favourites-tab">
            <a href="/favouriteSoundtracks">Favourite Soundtracks</a>
        </div>
        <div class="favourites-tab">
            <a href="/mysoundtracks">My Soundtracks</a>
        </div>
        <div class="favourites-tab">
            <a href="/favouritePlaylists">Favourite Playlists</a>
        </div>
        <div class="favourites-tab">
            <a href="/myplaylists">My Playlists</a>
        </div>
    </div>
    <div class="container">
        <div class="forminput">
            <form method="post" action="/myplaylists">
                <input type="text" name="title" value="${title}">
                <input type="submit">
            </form>
        </div>

        <div class="searchResults">


            <c:forEach items="${searchList}" var="playlist">
                <div class="playlists">
                    <div class="leftPlaylist">
                        <a href="/playlist/${playlist.id}">${playlist.title}
                        </a>

                    </div>
                    <div class="rightPlaylist">
                        <div class="inline-block rate-block" id="Pdiv${playlist.id}">

                            <button name="L${playlist.id}" class="rate like-button passive"><img
                                    src="https://cdn.discordapp.com/attachments/564049428231618570/566110050838380568/Like1.png"/>
                            </button>
                            <button name="D${playlist.id}" class="rate dislike_button passive"><img
                                    src="https://cdn.discordapp.com/attachments/564049428231618570/566111383368761354/dislike.png"/>
                            </button>

                        </div>
                        <a href="/deleteplaylist/${playlist.id}/userPlaylistsPage"><img
                                style="width: 20px; height: 20px;"
                                src="https://www.shareicon.net/download/2015/08/01/78566_cancel_512x512.png"/></a>

                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <a href="/editplaylist/0">Create new playlist</a>
</div>
<script>
    var userId = <%=user.getId()%>;

    function setDiv(id, rate) {
        var div = $(id);
        if (rate == 1) {
            div.children().first().attr("class", "rate like-button active");
            div.children().last().attr("class", "rate dislike-button passive")
        } else {
            if (rate == 0) {
                div.children().first().attr("class", "rate like-button passive");
                div.children().last().attr("class", "rate dislike-button passive")
            } else {
                div.children().first().attr("class", "rate like-button passive");
                div.children().last().attr("class", "rate dislike-button active")
            }
        }
    }

    function postrateS(soundtrackId, rate) {
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/ratesoundtrack/" + soundtrackId + "/" + userId + "/" + rate,
            success: function (response) {
                setDiv("#Sdiv" + soundtrackId, JSON.stringify(response));
            },
            error: function () {
                alert("The is some problems")
            }
        })
    }

    function setrateS(soundtrackId, userId) {
        var rate;
        $.get("http://localhost:8080/getsoundtrackrate/" + soundtrackId + "/" + userId, {}, function (response) {
            rate = JSON.stringify(response);
            setDiv("#Sdiv" + soundtrackId, rate);
        });
    }

    function postrateP(playlistId, rate) {
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/rateplaylist/" + playlistId + "/" + userId + "/" + rate,
            success: function (response) {
                setDiv("#Pdiv" + playlistId, JSON.stringify(response));
            },
            error: function () {
                alert("The is some problems")
            }
        })
    }

    function setrateP(playlistId, userId) {
        var rate;
        $.get("http://localhost:8080/getplaylistrate/" + playlistId + "/" + userId, {}, function (response) {
            rate = JSON.stringify(response);
            setDiv("#Pdiv" + playlistId, rate);
        });
    }

    $("div.rate-block").each(function () {
        var type = this.id.substring(0, 1);
        var id = this.id.substring(4);
        if (type === "P") {
            setrateP(id, userId);
        } else {
            setrateS(id, userId);
        }
    });
    $("button.rate").click(function () {
        var rate = 1;
        var method = this.name.substring(0, 1);
        if (method === "D") {
            rate = -1;
        } else {
            rate = 1;
        }
        if ($(this).hasClass("active")) {
            rate = 0
        }
        var id = this.name.substring(1);
        var type = $(this).parent().attr("id").substring(0, 1);
        if (type === "P") {
            postrateP(id, rate);
        } else {
            postrateS(id, rate);
        }
    });
</script>
</body>

</html>