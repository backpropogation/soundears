<%--@elvariable id="soundtrackTitle" type="java.lang.String"--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8"%>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        #content{
            font-size: 50px;
            width: auto;
            text-align: center;
            margin-top: 201px;
        }
        input.playlisttitle{
            height: 35px;
            font-size: 20px;
        }
        input.playlistdesc{
            height: 35px;
            font-size: 16px;
            width: 500px;
        }
        input#confirm{
            margin-top: 20px;
            height: 30px;
            font-size: 20px;
            padding: 10px 20px 35px 20px;
            border: 1px solid darkgray;
            border-radius: 5px;
            background-color: white;
        }
        h2{
            margin-top: 20px;
        }
    </style>
</head>

<body>
<%@include file="header.jsp"%>
<div id="content">
    <%--@elvariable id="playlist" type="com.pokorili.musicOn.entity.Playlist"--%>
    <form:form action="/editplaylist" method="post" modelAttribute="playlist">
        <h2>Название: </h2>
        <form:input type="hidden" path="id" value="${playlist.id}" />
        <form:input cssClass="playlisttitle" path="title" value="${playlist.title}"/>
        <h2>Описание: </h2>
        <form:input cssClass="playlistdesc" path="description" value="${playlist.description}"/><br/>
        <input id="confirm" type="submit" value="Подтвердить" />
    </form:form>
</div>
</body>

</html>