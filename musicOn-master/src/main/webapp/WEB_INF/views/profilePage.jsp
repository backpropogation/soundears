<%@page contentType="text/html;charset=UTF-8"%>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
</head>

<body>
<%@include file="header.jsp"%>
<style>
    .userInfo {
        margin: 30px;
    }
    .nickname {
        color: #41435b;
        margin: 20px;
    }
    .email {
        margin: 20px;
    }
    .changeButton {
        padding: 5px;
        border: 2px solid #41435b;
    }
    .info-block {
        color: #00b900;
    }
    .red{
        border-color: red;
        color: red;
    }
    <%
    if (request.getAttribute("infoMessage") != null) {
    %>
    .info-block {
        background-color: #adff98;
        padding: 10px;
    }
    <%
    }
    %>
</style>
<div class="mainContent">
    <div class="info-block" align="center">
        <p>${infoMessage}</p>
    </div>
    <div class="userInfo">
        <h1 class="nickname"><%=user.getNickname()%></h1>
        <p class="label">Your email:</p>
        <p class="email"><%=user.getEmail()%></p>
        <a class="changeButton" href="/changeNickname">Сменить никнейм</a>
        <a class="changeButton" href="/changeEmail">Сменить email</a>
        <a class="changeButton" href="/changePassword">Сменить пароль</a>
        <%
            if(user.getStatus().equals("Admin")) {
        %>
        <a class="changeButton" href="/adsmanagement">Управление рекламой</a>
        <%
            }
        %>
        <a class="changeButton red" href="/deleteuser"><b class="red">Удаление аккаунта</b></a>

    </div>
</div>
</body>

</html>