<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8"%>
<html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        .free-adv{
            margin-top: 100px;
            width: auto;
            text-align: center;
        }
        .adv {
            border: 1px solid darkgray;
            text-align: center;
        }
        .adv p {
            font-size: 20px;
        }
        .deleteButton {
            color: red;
            border: 1px solid red;
            border-radius: 5px;
            padding: 10px;
            font-size: 20px;
            background-color: white;
        }
    </style>
</head>

<body>
<%@include file="header.jsp"%>

<div class="main-content">
    <div class="listHolder">
        <%--@elvariable id="adverts" type="com.pokorili.musicOn.entity.Advert[]"--%>
        <c:forEach items="${adverts}" var="advert">
            <b><h2 style="text-align: center">Place №${advert.advertPlace.id}:</h2></b>
            <div class="adv">
                <b><p>Picture:</p></b>
                <img style="width: 15%; height: 15%" src="http://localhost:4000/getadvpic/${advert.id}"/>
                <p><b>Description:</b> ${advert.description}</p>
                <p><b>Link:</b> ${advert.advLink}</p>
                <button style="margin: 5px 0;" class="deleteButton" id="Del${advert.advertPlace}" onclick="delAdv(${advert.id}, ${advert.advertPlace.id})">Delete</button>
            </div>
        </c:forEach>
        <%--@elvariable id="freePlaces" type="com.pokorili.musicOn.entity.AdvertPlace[]"--%>
        <c:forEach items="${freePlaces}" var="place">
            <div class="free-adv">
                <h2 style="font-size: 50px;">Место №${place.id}</h2>
                <a style="border: 2px black solid;padding: 6px;" href="http://localhost:4000/addadvert/${place.id}"><i style="font-size: 30px">Добавить рекламу</i></a>
            </div>
        </c:forEach>
    </div>
</div>
<script>
    function delAdv(advertId, advertPlaceId) {
        $.get(
            "http://localhost:4000/deactivateadvert/" + advertId + "/" + advertPlaceId,
            {},
            function () {
                window.location.replace("http://localhost:4000/adsmanagement")
            }
        )
    }
</script>
</body>

</html>