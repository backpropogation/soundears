<%@ page import="org.springframework.web.multipart.MultipartFile" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        .multipleFiles{
            width: 100%;
            margin: 70px auto;
            text-align: center;
        }
        h2{
            margin-top: 40px;
            font-size: 35px;
        }
        input[type='text']{
            font-size: 20px;
            margin-left: 15px;
        }
        select{
            font-size: 20px;
            margin-left: 15px;
        }
        input#sendSong{
            margin-top: 20px;
            font-size: 30px;
        }
    </style>
</head>

<body>
<%@include file="header.jsp" %>

<div class="multipleFiles">
    <form id="sendNames" name="songs" method="post">
        <c:forEach items="${files}" var="file">
            <h2>${file.getOriginalFilename()}</h2>
            <input class="author" type="text" required placeholder="Автор"/>
            <input class="title" type="text" required placeholder="Название"/>
            <select class="genres">
                <c:forEach items="${genres}" var="genre">
                    <option value="${genre.id}">${genre.title}</option>
                </c:forEach>
            </select>
        </c:forEach>
        <br/>
        <input id="sendSong" type="submit" value="Отправить" onclick="send()"/>

    </form>
    <script>
        function send() {
            var authors = [];
            $('.author').each(function () {
                authors.push($(this).val());
                if ($(this).val() == ""){
                    return;
                }
            });
            var titles = [];
            $('.title').each(function () {
                titles.push($(this).val());
                if ($(this).val() == ""){
                    return;
                }
            });
            var genreIds = [];
            var genreSelects = document.getElementsByClassName('genres');
            for (var i = 0; i < genreSelects.length; i++) {
                var a = genreSelects[i];
                var genreId = a.options[a.selectedIndex].value;
                genreIds.push(genreId);
            }

            var data = JSON.stringify({"authors": authors, "titles": titles, "genreIds": genreIds});
            var xhr = new XMLHttpRequest();
            var url = "http://localhost:4000/namesongs";

            xhr.open("POST", url, false);
            xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
            xhr.onreadystatechange = function () {
                window.location.replace("http://localhost:4000/mysoundtracks");
            };
            xhr.send(data);
            event.preventDefault();

        }
    </script>
</div>
</body>
</html>