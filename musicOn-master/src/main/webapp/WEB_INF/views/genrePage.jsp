<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        a:visited {
            color: black;
        }

        a {
            color: black;
            text-decoration: none;
        }

        .searchType {
            margin: 10px;
            width: fit-content;
            border: 2px solid black;
            padding: 3px;
            display: inline-block;
        }

        .searchChoose {
            text-align: center;
        }

        .container {
            margin: 0 100px;
        }

        .forminput {
            margin-top: 20px;
        }

        .searchResults {
            margin-top: 20px;
        }

        .songs {
            padding: 5px;
            margin-top: 12px;
            border: 0px;
            width: 700px;
            height: 65px;
            background-color: #f1f3f4;
            border-radius: 20px;
        }

        .playlists {
            padding: 5px;
            margin-top: 4px;
            border: 0px;
            width: 600px;
            height: 40px;
            border-radius: 20px;
            background-color: #f1f3f4;
        }

        .playlists > * {
            margin-top: 10px;
        }

        .passive {
            color: darkblue;
        }

        .active {
            background-color: red;

        }

        .leftSong {
            display: inline-block;
        }

        .rightSong {
            display: inline-block;
            vertical-align: top;
            margin-top: 2px;
        }

        .titleAndAuthor {
            width: 300px;
            font-size: 20px;
            text-align: center;
            margin-bottom: 5px;
        }

        audio {
            width: 350px;
            height: 30px;
        }

        button:active {
            border: none;
        }

        .rightSongButtons > * {
            margin-left: 20px;
            height: 20px;

        }

        .addToPlaylist {
            align-content: center;
            text-align: center;
            margin-top: 10px;
        }

        .leftPlaylist {
            display: inline-block;
            vertical-align: top;
            width: 400px;
        }

        .rightPlaylist {
            display: inline-block;
            margin-left: 5px;
        }

        .leftPlaylist a {
            margin-left: 90px;
        }

        .rate {
            border: none;
        }

        button.dislike-button.active {
            background-color: black;
        }

        .links {
            font-size: 40px;
            display: inline-block;
            margin: 0 40px;
        }

        #genres {
            margin-top: 30px;
        }

        /*#songs{*/
        /*width: 720px;*/
        /*margin: 0 auto;*/

        /*}*/
        #mainContent{
            width: 80%;
            margin: 0 auto;
        }
    </style>
</head>

<body>
<script>
    var genreId = <c:out value="${searchList[0].genre.id}"/>;
    if (genreId === undefined && window.location != "http://localhost:4000/genres")
        window.location.assign("http://localhost:4000/genres");
    if (genreId !== undefined && window.location != "http://localhost:4000/genres/" + genreId) {
        window.location.assign("http://localhost:4000/genres/" + genreId);
    }

</script>
<%@include file="header.jsp" %>

<script>
    function generate(i) {
        var userid = <%=user.getId()%>;
        var array = document.getElementsByClassName('addToPlaylist');
        if (array[i].innerHTML != "") {
            array[i].innerHTML = "";
            return;
        }
        if (userid == null) {
            array[i].innerHTML = "<h3>You are not logged in!</h3>";
        } else {
            array = document.getElementsByClassName('addToPlaylist');
            for (var x = 0; x < array.length; x++) {
                array[x].innerHTML = "";
            }
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("GET", 'http://localhost:8080/playlists/' + userid, false);
            xmlHttp.send(null);
            var inputarray = JSON.parse(xmlHttp.responseText);
            if (inputarray.length != 0) {
                var playlistArray = "";
                for (var j = 0; j < inputarray.length; j++) {
                    playlistArray += "<option value=" + inputarray[j].id + ">"
                        + inputarray[j].title + "</option>\n";
                }
                array[i].innerHTML = "<select id='select'>" + playlistArray + "</select>" +
                    "<button onclick='sendAddRequest(" + i + ")'>Send</button>";
            } else {
                array[i].innerHTML = "<h3>You have no playlists at all!</h3>";
            }
        }
    }

    function sendAddRequest(i) {
        var a = document.getElementById("select");
        var playlistId = a.options[a.selectedIndex].value;
        var songId = document.getElementsByClassName('songId')[i].innerHTML;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", 'http://localhost:8080/playlistsoundtrack/' +
            playlistId + "/" + songId, false);
        xmlHttp.send(null);
        var array = document.getElementsByClassName('addToPlaylist');
        if (xmlHttp.response == "") {
            xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", 'http://localhost:8080/addplaylistsoundtrack/' +
                playlistId + "/" + songId, false);
            xmlHttp.send(null);
            array[i].innerHTML = "<h3>The song was added to this playlist!</h3>";
            //soundtrackplaylist doesnt exist
        } else {
            array[i].innerHTML = "<h3>The song is already in this playlist!</h3>";
            //soundtrackplaylists exists
        }
    }
</script>


<div class="input-form" align="center">
    <%--@elvariable id="newUser" type="com.pokorili.musicOn.entity.Visitor"--%>
    <div id="genres">
        <c:forEach items="${genres}" var="genre">
            <div class="links"><a class="genre" href="/genres/${genre.id}">${genre.title}</a></div>
        </c:forEach>
    </div>
</div>
<div id="mainContent">
    <div id="songs">
        <%
            int i = 0;
        %>

        <c:forEach items="${searchList}"
                   var="soundtrack"><%--@elvariable id="searchtype" type="java.lang.String"--%>
            <c:set var="soundtrackauthorid" value="${soundtrack.visitor.id}"/>
            <div class="songs">
                <div class="leftSong">
                    <div class="titleAndAuthor">
                        <b>  ${soundtrack.author} - ${soundtrack.title} </b>
                    </div>
                    <audio controls preload="auto">
                        <source src="http://localhost:4000/getsong/${soundtrack.id}" type="audio/mpeg">
                    </audio>
                </div>
                <div class="rightSong">
                    <div class="rightSongButtons">

                        <a href="/download/${soundtrack.id}"> <img
                                style="height: 25px;width: 25px;"
                                src="https://cdn4.iconfinder.com/data/icons/defaulticon/icons/png/256x256/arrow-down.png"></a>

                        <div class="inline-block rate-block" id="Sdiv${soundtrack.id}">
                            <button name="L${soundtrack.id}" class="rate like-button passive"><img
                                    src="https://cdn.discordapp.com/attachments/564049428231618570/566110050838380568/Like1.png"/>
                            </button>
                            <button name="D${soundtrack.id}" class="rate dislike_button passive"><img
                                    src="https://cdn.discordapp.com/attachments/564049428231618570/566111383368761354/dislike.png"/>
                            </button>
                        </div>
                        <button style="height: 35px;width: 35px; background-color: #f1f3f4;border: none;"
                                onclick="generate(<%=i%>)"><img style="height: 25px;width: 25px;"
                                                                src="http://www.free-icons-download.net/images/a-cross-is--symbol-icon-49700.png"/>
                        </button>
                        <%
                            Long id = (Long) pageContext.getAttribute("soundtrackauthorid");
                            if (user.getStatus().equals("Admin") ||
                                    user.getId() == id) {

                        %>
                        <a href="/deletesoundtrack/${soundtrack.id}/genrePage/${soundtrack.genre.id}/123"><img
                                style="width: 20px; height: 20px;"
                                src="https://www.shareicon.net/download/2015/08/01/78566_cancel_512x512.png"/></a>

                        <%
                            }
                        %>
                    </div>
                    <div class="addToPlaylist"></div>
                </div>
                <div class="songId" style="display: none">${soundtrack.id}</div>

            </div>
            <%
                i++;
            %>
        </c:forEach>
    </div>
</div>
<script>
    var userId = <%=user.getId()%>;

    function setDiv(id, rate) {
        var div = $(id);
        if (rate == 1) {
            div.children().first().attr("class", "rate like-button active");
            div.children().last().attr("class", "rate dislike-button passive")
        } else {
            if (rate == 0) {
                div.children().first().attr("class", "rate like-button passive");
                div.children().last().attr("class", "rate dislike-button passive")
            } else {
                div.children().first().attr("class", "rate like-button passive");
                div.children().last().attr("class", "rate dislike-button active")
            }
        }
    }

    function postrateS(soundtrackId, rate) {
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/ratesoundtrack/" + soundtrackId + "/" + userId + "/" + rate,
            success: function (response) {
                setDiv("#Sdiv" + soundtrackId, JSON.stringify(response));
            },
            error: function () {
                alert("The is some problems")
            }
        })
    }

    function setrateS(soundtrackId, userId) {
        var rate;
        $.get("http://localhost:8080/getsoundtrackrate/" + soundtrackId + "/" + userId, {}, function (response) {
            rate = JSON.stringify(response);
            setDiv("#Sdiv" + soundtrackId, rate);
        });
    }

    function postrateP(playlistId, rate) {
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/rateplaylist/" + playlistId + "/" + userId + "/" + rate,
            success: function (response) {
                setDiv("#Pdiv" + playlistId, JSON.stringify(response));
            },
            error: function () {
                alert("The is some problems")
            }
        })
    }

    function setrateP(playlistId, userId) {
        var rate;
        $.get("http://localhost:8080/getplaylistrate/" + playlistId + "/" + userId, {}, function (response) {
            rate = JSON.stringify(response);
            setDiv("#Pdiv" + playlistId, rate);
        });
    }

    $("div.rate-block").each(function () {
        var type = this.id.substring(0, 1);
        var id = this.id.substring(4);
        if (type === "P") {
            setrateP(id, userId);
        } else {
            setrateS(id, userId);
        }
    });
    $("button.rate").click(function () {
        var rate = 1;
        var method = this.name.substring(0, 1);
        if (method === "D") {
            rate = -1;
        } else {
            rate = 1;
        }
        if ($(this).hasClass("active")) {
            rate = 0
        }
        var id = this.name.substring(1);
        var type = $(this).parent().attr("id").substring(0, 1);
        if (type === "P") {
            postrateP(id, rate);
        } else {
            postrateS(id, rate);
        }
    });
</script>
</body>
</html>