<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8"%>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        .register-form {
            width: 500px;
            margin: 100px auto;
            border: 2px solid darkgray;
            border-radius: 5px;
        }
        .inputField {
            margin: 20px;
        }
        .red {
            color:red;
        }
        p.label {
            text-align: left;
            font-size: 30px;

        }
        .inputText {
            margin-top: 10px;
            font-size: 30px;
            height: 30px;
        }

        #login-button {
            padding: 15px;
            float: right;
            width: 170px;
            background-color: white;
            border: 1px darkgray solid;
            border-radius: 5px;
        }

        div .red {
            border: 0px solid red;
            border-radius: 5px;
            background-color: #ff9b90;
        }

        .temp {
            height: 60px;
        }
        <%
        if (request.getAttribute("errMessage") != null) {
        %>
        div .red p {
            padding: 5px;
        }

        <%
        }
        %>

    </style>
</head>

<body>
<%@include file="header.jsp"%>
<div class="register-form" align="center">
    <%--@elvariable id="newUser" type="com.pokorili.musicOn.entity.Visitor"--%>
    <form:form method="post" action="/register" modelAttribute="newUser">
        <div class="inputField">
            <label>
                <p class="label">Электронная почта:</p>
                <form:input type="text" path="email" cssClass="inputText"/>
            </label>
        </div>
        <div class="inputField">
            <label>
                <p class="label">Никнейм:</p>
                <form:input type="text" path="nickname" cssClass="inputText"/>
            </label>
        </div>
        <div class="inputField red">
            <p class="red">${errMessage}</p>
        </div>
        <div class="inputField">
            <label>
                <p class="label">Пароль:</p>
                <form:input type="password" path="password" cssClass="inputText"/>
            </label>
        </div>
        <div class="temp">
        <div class="inputField">
            <input type="submit" value="Зарегистрироваться" id="login-button">
        </div>
        </div>
    </form:form>
</div>
</body>

</html>