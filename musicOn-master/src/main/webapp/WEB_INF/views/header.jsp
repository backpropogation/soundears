<%@ page import="com.pokorili.musicOn.entity.Visitor" %>
<%@page contentType="text/html;charset=UTF-8" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    * {
        padding: 0;
        margin: 0;
    }
    body {
        min-width: 777px;
    }

    a:visited {
        color: #000000;
    }

    a {
        color: #000000;
        text-decoration: none;
    }

    input#mainSearchLine {
        width: 450px;
        height: 30px;
    }

    input#mainSearchButton {
        height: 30px;
        width: 60px;
        background-color: white;
        border: 1px gray solid;
    }

    .header {
        min-height: 100px;
        max-height: 400px;
        height: fit-content;
        border: 1px solid darkgray;
        background-color: #ffbea3;
        color: white;
    }

    .icon {
        width: 10%;
        vertical-align: middle;
        min-width: fit-content;
        margin: 30px;
    }

    .searchInput {
        width: 35%;
        min-width: 550px;
        margin: 10px;
        vertical-align: middle;
        font-size: 30px;
    }

    .inline-block {
        display: inline-block;
    }

    .right-block div {
        margin: 0 10px;

    }

    .right-block {
        margin-left: 20px;
        height: 100px;
        font-size: 20px;
        font-weight: bold;
        vertical-align: middle;
        align-content: center;
    }

    .temp-block {
        text-align: center;
    }

    .login {
        vertical-align: middle;
        padding-top: 40px;
    }

    .right-block a {
        padding: 20px;
    }
    .adv-div {
        text-align: center;
    }
</style>
<div class="adv-div" id="1">
    <a>
        <img/><br>
        <span class="adv-desc" id="1d"></span>
    </a>
</div>
<div class="header">
    <div class="icon inline-block">
        <h1><a href="/">MusicOn</a></h1>
    </div>
    <div class="searchInput inline-block">
        <form method="get" action="/search">
            <input type="text" name="title" value="${title}" id="mainSearchLine"/>
            <input type="submit" value="Найти" id="mainSearchButton"/>
            <input type="hidden" name="searchtype" value="soundtrackByTitle">
        </form>
    </div>

    <div class="right-block inline-block">
        <%
            Visitor user = (Visitor) session.getAttribute("user");
            if (user.getStatus().equals("UnAuth")) {
        %>
        <div class="temp-block">
            <div class="login inline-block">
                <a href="/genres">Жанры</a>
            </div>
            <div class="login inline-block">
                <a href="/login">Войти</a>
            </div>
            <div class="login inline-block">
                <a href="/register">Регистарция</a>
            </div>
        </div>
        <%
        } else {
        %>

        <div class="login inline-block">
            <a href="/genres">Жанры</a>
        </div>
        <div class="inline-block login">
            <a href="/upload">Загрузить трек</a>
        </div>
        <div class="inline-block login">
            <a href="/favouriteSoundtracks">Моя музыка</a>
        </div>
        <div class="inline-block login">
            <a href="/profile"><%=user.getNickname()%>
            </a>
        </div>
        <div class="inline-block login">
            <a href="/logout">Выйти</a>
        </div>
        <%
            }
        %>
    </div>

</div>
<script>


    function postAdv(advert) {
        var id = advert.advertPlace.id;
        var div = document.getElementById(id);
        var a = div.children[0];
        a.href = advert.advLink;
        a.children[1].innerHTML = advert.description;
        var img = a.children[0];
        img.src = "http://localhost:4000/getadvpic/" + advert.id;
    }

    function getAdvOnPlace(id) {
        $.get(
            "http://localhost:8080/getadvertbyplace/" + id,
            {},
            function (response) {
                var advert = response;
                if (advert != undefined) {
                    postAdv(advert);
                }
            }
        )
    }

    $.each($(".adv-div"), function () {
        var id = this.id;
        getAdvOnPlace(id);
    });


</script>