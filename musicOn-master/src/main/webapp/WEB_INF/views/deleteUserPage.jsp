<%--@elvariable id="soundtrackTitle" type="java.lang.String"--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        #deleteform {
            font-size: 50px;
            width: auto;
            margin: 300px auto;
            align-content: center;
            text-align: center;
        }
    </style>
</head>


<body>
<%@include file="header.jsp" %>
<div id="deleteform">
    <form action="/deleteuser" method="post">
        <input style="font-size: 50px; border: 2px red solid; margin: 0 20px; padding: 10px;" type="submit" value="Подтвердить удаление"/>
        <a href="/profile">Отменить</a>
    </form>
</div>
</body>

</html>