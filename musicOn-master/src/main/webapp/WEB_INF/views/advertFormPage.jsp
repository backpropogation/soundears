<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <title>MusicOn</title>
    <meta charset="UTF-8"/>
    <style>
        #form{
            margin-top: 100px;
            width: auto;
            font-size: 50px;
            text-align: center;
        }
        .margin{
            margin-top: 20px;
        }
        input#addAdvert{
            margin-top: 21px;
            font-size: 31px;
        }
        input.advin{
            height: 35px;
            width: 303px;
            margin-left: 13px;
        }
    </style>
</head>

<body>
<%@include file="header.jsp" %>

<div class="multipleFiles">
    <%--@elvariable id="advert" type="com.pokorili.musicOn.entity.Advert"--%>
    <form:form action="/addadvert" method="post" modelAttribute="advert" enctype="multipart/form-data">
        <div id="form">
            <div class="margin"><i>Изображение:  </i><input name="img" type="file"/><br/></div>
            <div class="margin"><i>Рекламная ссылка</i><form:input path="advLink" required="required" type="text" cssClass="advin"/><br/></div>
            <div class="margin"><i>Описание </i><form:input path="description" required="required" type="text" cssClass="advin"/></div>
            <input type="hidden" name="placeId" value="${placeId}">
            <input id="addAdvert" type="submit" value="Добавить"/>
        </div>
    </form:form>
</div>
</body>
</html>