package com.pokorili.musicOn.service;


import com.google.gson.Gson;
import com.pokorili.musicOn.entity.Playlist;
import com.pokorili.musicOn.entity.Soundtrack;
import com.pokorili.musicOn.entity.Visitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class PlaylistService {
    @Autowired
    ConnectionService connectionService;

    public Playlist getPlaylist(Long id){
        Playlist playlist;
        Gson gson = new Gson();
        try {
            String link = "http://localhost:8080/playlist/" + id;
            playlist = gson.fromJson(connectionService.sendRequest(link,"GET", null, null), Playlist.class);
        } catch (IOException e) {
            e.printStackTrace();
            playlist = null;
        }
        return playlist;
    }

    public Playlist[] getPlaylistsByVisitorId(Long id) throws IOException {
        Playlist[] playlists;
        Gson gson = new Gson();
        playlists = gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/playlists/" + id, "GET", null, null), Playlist[].class);
        return  playlists;
    }

    public Playlist[] getPlaylistByVisitorAndTitle(Long id, String title) throws IOException {
        Gson gson = new Gson();
        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        Playlist[] playlists= gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/playlists/" + id, "GET", params, null), Playlist[].class);
        return playlists;
    }

    public void deletePlaylist(Long id) throws IOException {
        connectionService.sendRequest("http://localhost:8080/deleteplaylist/" + id,
                "GET", null, null);
    }

    public Playlist[] getPlaylists(String title) {
        Playlist[] playlists;
        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        Gson gson = new Gson();
        try {
            String link = "http://localhost:8080/playlists";
            playlists = gson.fromJson(connectionService.sendRequest(link,"GET", params, null), Playlist[].class);
        } catch (IOException e) {
            e.printStackTrace();
            playlists = new Playlist[0];
        }
        return playlists;
    }

    public void deleteSoundtrackFromPlaylist(Long songId, Long playlistId) throws IOException {
        connectionService.sendRequest("http://localhost:8080/deleteplaylistsoundtrack/" + playlistId + "/" +
                songId,
                "GET", null, null);
    }

    public Playlist editPlaylist(String title, String description, Long id) throws IOException {
        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        params.put("description", description);
        Gson gson = new Gson();
        return gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/playlist/" + id,
                "POST", params, null), Playlist.class);
    }

    public void addNewPlaylist(Playlist playlist) throws IOException {
        connectionService.sendRequest(
                "http://localhost:8080/addplaylist/", "POST",
                null, playlist);
    }

    //TODO
    public Playlist[] getLikedPlaylists(Visitor user) throws IOException {
        Gson gson = new Gson();
        Playlist[] playlists = gson.fromJson(connectionService.sendRequest("http://localhost:8080/likedplaylists", "POST", null,  user), Playlist[].class);
        return playlists;
    }

}
