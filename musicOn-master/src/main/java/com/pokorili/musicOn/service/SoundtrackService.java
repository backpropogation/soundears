

package com.pokorili.musicOn.service;

import com.google.gson.Gson;
import com.pokorili.musicOn.entity.Soundtrack;
import com.pokorili.musicOn.entity.Visitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Service
public class SoundtrackService {
    @Autowired
    ConnectionService connectionService;

    public Soundtrack[] getSoundtracksByPlaylist(Long id) throws IOException {
        Soundtrack[] soundtracks;
        String link = "http://localhost:8080/soundtrackonplaylist/" + id;
        Gson gson = new Gson();
        soundtracks = gson.fromJson(connectionService.sendRequest(link, "GET", null, null), Soundtrack[].class);
        return soundtracks;
    }

    public Soundtrack[] getSoundtracksBy(String searchtype, String variable) throws IOException {
        Soundtrack[] soundtracks;
        Map<String, String> params = new HashMap<>();
        params.put(searchtype, variable);
        Gson gson = new Gson();
        String link = "http://localhost:8080/soundtrack";
        soundtracks = gson.fromJson(connectionService.sendRequest(link,"GET", params, null), Soundtrack[].class);
        return soundtracks;
    }

    public Soundtrack getSoundtrackById(Long id) throws IOException {
        Gson gson = new Gson();
        Soundtrack soundtrack = gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/soundtrack/" + id, "GET", null, null), Soundtrack.class);
        return soundtrack;
    }

    public Soundtrack[] getSoundtrackByVisitor(Long id) throws IOException {
        Gson gson = new Gson();
        Soundtrack[] soundtrack = gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/soundtracks/" + id, "GET", null, null), Soundtrack[].class);
        return soundtrack;
    }

    public Soundtrack[] getSoundtrackByVisitorAndTitle(Long id, String title) throws IOException {
        Gson gson = new Gson();
        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        Soundtrack[] soundtrack = gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/soundtracks/" + id, "GET", params, null), Soundtrack[].class);
        return soundtrack;
    }

    public Soundtrack[] getSoundtracksByGenreId(Long id) throws IOException {
        Gson gson = new Gson();
        Map<String, String> params = new HashMap<>();
        //
        params.put("genreId", String.valueOf(id));
        Soundtrack[] soundtracks = gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/soundtracks/", "GET", params, null),
                Soundtrack[].class);
        return soundtracks;
    }

    public void deleteSoundtrack(Long id) throws IOException {
        Gson gson = new Gson();
        Soundtrack soundtrack = gson.fromJson(connectionService.sendRequest(
                "http://localhost:8080/soundtrack/" + id, "GET", null, null), Soundtrack.class
        );
        connectionService.sendRequest("http://localhost:8080/deletesoundtrack/" + id, "GET", null, null);
    }

    public Soundtrack addSoundtrack(Soundtrack soundtrack) throws IOException {
        Soundtrack ret;
        Gson gson = new Gson();
        ret = gson.fromJson(connectionService.sendRequest("http://localhost:8080/addsoundtrack", "POST", null,  soundtrack), Soundtrack.class);
        return ret;
    }
    //TODO
    public Soundtrack[] getLikedSoundtracks(Visitor user) throws IOException {
        Gson gson = new Gson();
        Soundtrack[] soundtracks = gson.fromJson(connectionService.sendRequest("http://localhost:8080/likedsoundtracks", "POST", null,  user), Soundtrack[].class);
        return soundtracks;
    }
    //TODO
    public Soundtrack[] getHistory(Long id) {
        Soundtrack[] soundtracks = new Soundtrack[0];
        return soundtracks;
    }

}
