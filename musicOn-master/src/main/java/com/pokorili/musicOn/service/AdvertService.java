package com.pokorili.musicOn.service;

import com.google.gson.Gson;
import com.pokorili.musicOn.entity.Advert;
import com.pokorili.musicOn.entity.AdvertPlace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class AdvertService {

    @Autowired
    UserService userService;

    @Autowired
    ConnectionService connectionService;

    public AdvertPlace getAdvertPlace(Long id) throws IOException {
        Gson gson = new Gson();
        AdvertPlace advertPlace = gson.fromJson(connectionService.sendRequest("http://localhost:8080/advertPlaces/" + id, "GET", null, null), AdvertPlace.class);
        return advertPlace;
    }

    public Advert getAdvertById(Long id) throws IOException {
        Gson gson = new Gson();
        Advert advert = gson.fromJson(connectionService.sendRequest("http://localhost:8080/advert/" + id, "GET", null, null), Advert.class);
        return advert;
    }
    public Advert addAdvert(Advert advert) throws IOException {
        Map<String,String> params = new HashMap<>();
        params.put("isFree", "false");
        Gson gson = new Gson();
        advert = gson.fromJson(connectionService.sendRequest("http://localhost:8080/addadvert", "POST", null, advert), Advert.class);
        connectionService.sendRequest("http://localhost:8080/advertPlaces/" + advert.getAdvertPlace().getId(), "POST", params, null);
        return advert;
    }
    public Advert[] getActiveAdverts() throws IOException {
        Gson gson = new Gson();
        Advert[] adverts = gson.fromJson(connectionService.sendRequest("http://localhost:8080/activeadverts", "GET", null, null), Advert[].class);
        return adverts;
    }
    public AdvertPlace[] getFreeAdvertPlaces() throws IOException {
        Gson gson = new Gson();
        AdvertPlace[] advertPlaces = gson.fromJson(connectionService.sendRequest("http://localhost:8080/freeplaces", "GET", null, null), AdvertPlace[].class);
        return advertPlaces;
    }
    public void deleteAdvert(Long advertId, Long advertPlaceId) throws IOException {
        Map<String,String> params = new HashMap<>();
        params.put("isFree", "true");
        Gson gson = new Gson();
        gson.fromJson(connectionService.sendRequest("http://localhost:8080/deactivateadvert/" + advertId, "GET", null, null), Advert.class);
        connectionService.sendRequest("http://localhost:8080/advertPlaces/" + advertPlaceId, "POST", params, null);
    }
}
