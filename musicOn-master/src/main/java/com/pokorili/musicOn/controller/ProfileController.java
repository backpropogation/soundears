package com.pokorili.musicOn.controller;

import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.ConnectionService;
import com.pokorili.musicOn.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;


@Controller
@SessionAttributes("user")
public class ProfileController {

    @Autowired
    UserService userService;

    @Autowired
    ConnectionService connectionService;

    @ModelAttribute("newUser")
    public Visitor createNewVisitor() {
        return new Visitor("UnAuth");
    }

    @GetMapping("/profile")
    public String getProfilePage() {
        return "profilePage";
    }

    @GetMapping("/changeEmail")
    public String getChangeEmailPage(Model model) {
        model.addAttribute("setting", "email");
        return "changeProfileSettingsPage";
    }

    @PostMapping("/changeEmail")
    public String changeEmail(@SessionAttribute("user") Visitor curUser, @ModelAttribute("newUser") Visitor newUser, Model model) {
        Visitor checkVisitor = userService.getVisitorByEmail(newUser.getEmail());
        if (checkVisitor != null) {
            model.addAttribute("errMessage", "Эта электронная почта уже привязана к другому аккаунту");
            model.addAttribute("setting", "email");
            return "changeProfileSettingsPage";
        } else {
            curUser.setEmail(newUser.getEmail());
            userService.changeVisitorProperty(curUser.getId(), "email", newUser.getEmail());
            model.addAttribute("infoMessage", "Адрес электронной почты успешно изменён");
            connectionService.sendEmail(curUser.getEmail(), "Email change" ,"This email was chosen as a profile email of " + curUser.getNickname() + " on MusicOn");
            return "profilePage";
        }
    }

    @GetMapping("/changeNickname")
    public String getChangeNicknamePage(Model model) {
        model.addAttribute("setting", "nickname");
        return "changeProfileSettingsPage";
    }

    @PostMapping("/changeNickname")
    public String changeNickname(@SessionAttribute("user") Visitor curUser, @ModelAttribute("newUser") Visitor newUser, Model model) {
        Visitor checkVisitor = userService.getVisitorByNickname(newUser.getNickname());
        if (checkVisitor != null) {
            model.addAttribute("errMessage", "Этот никнейм уже занят");
            model.addAttribute("setting", "nickname");
            return "changeProfileSettingsPage";
        } else {
            curUser.setNickname(newUser.getNickname());
            userService.changeVisitorProperty(curUser.getId(), "nickname", newUser.getNickname());
            model.addAttribute("infoMessage", "Никнейм успешно изменён!");
            connectionService.sendEmail(curUser.getEmail(), "Nickname Change" ,"Your nickname was changed!\nYour new nickname is: " + curUser.getNickname());
            return "profilePage";
        }
    }

    @GetMapping("/changePassword")
    public String getChangePasswordPage(Model model) {
        model.addAttribute("setting", "password");
        return "changeProfileSettingsPage";
    }

    @PostMapping("/changePassword")
    public String changePassword(@SessionAttribute("user") Visitor curUser, @ModelAttribute("newUser") Visitor newUser, Model model) {
        if (curUser.getPassword().equals(newUser.getEmail())) {
            if (newUser.getNickname().equals(newUser.getPassword())) {
                curUser.setPassword(newUser.getPassword());
                userService.changeVisitorProperty(curUser.getId(), "password", newUser.getPassword());
                connectionService.sendEmail(curUser.getEmail(), "Password change" ,"Your password was changed!\nYour new password is: " + curUser.getPassword());
                model.addAttribute("infoMessage", "Пароль успещно изменён!");
                return "profilePage";
            } else {
                model.addAttribute("errMessage", "Пароли не совпадают");
                model.addAttribute("setting", "password");
                return "changeProfileSettingsPage";
            }
        } else {
            model.addAttribute("errMessage", "Неправильный пароль");
            model.addAttribute("setting", "password");
            return "changeProfileSettingsPage";
        }
    }

    @GetMapping("/deleteuser")
    public String deleteUsr(){
        return "deleteUserPage";
    }

    @PostMapping("/deleteuser")
    public String deleteUser(@SessionAttribute("user") Visitor user) throws IOException {
        userService.deleteUser(user.getId());
        Visitor defaultUser = createNewVisitor();
        user.setStatus(defaultUser.getStatus());
        user.setStatus(defaultUser.getStatus());
        user.setNickname(defaultUser.getNickname());
        user.setEmail(defaultUser.getEmail());
        user.setPassword(defaultUser.getPassword());
        user.setId(defaultUser.getId());
        return "mainPage";
    }

}
