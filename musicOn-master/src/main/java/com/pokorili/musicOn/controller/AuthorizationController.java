package com.pokorili.musicOn.controller;

import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.ConnectionService;
import com.pokorili.musicOn.service.CryptService;
import com.pokorili.musicOn.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@SessionAttributes("user")
public class AuthorizationController {
    @ModelAttribute("user")
    public Visitor createVisitor() {
        return new Visitor("UnAuth");
    }

    @ModelAttribute("newUser")
    public Visitor createNewVisitor() {
        return new Visitor("UnAuth");
    }

    @Autowired
    UserService userService;

    @Autowired
    ConnectionService connectionService;

    @Autowired
    CryptService cryptService;

    @GetMapping("/login")
    public String getLoginPage() {
        return "loginPage";
    }

    @PostMapping("/login")
    public String checkLogin(@ModelAttribute("user") Visitor user, Model model) {
        Visitor searchVisitor = userService.getVisitorByNickname(user.getNickname());
        if (searchVisitor != null) {
            if (user.getPassword().equals(searchVisitor.getPassword())) {
                String message = userService.isUserValid(searchVisitor);
                if (message.equals("Ok")) {
                    user.setStatus(searchVisitor.getStatus());
                    user.setEmail(searchVisitor.getEmail());
                    user.setId(searchVisitor.getId());
                    return "mainPage";
                } else {
                    model.addAttribute("errMessage", message);
                    return "loginPage";
                }
            } else {
                model.addAttribute("errMessage", "Неправильно введён пароль");
                return "loginPage";
            }
        } else {
            model.addAttribute("errMessage", "Такой пользователь отсутствует в системе");
            return "loginPage";
        }
    }

    @GetMapping("/logout")
    public String logout(@SessionAttribute("user")Visitor user) {
        Visitor defaultUser = createVisitor();
        user.setStatus(defaultUser.getStatus());
        user.setNickname(defaultUser.getNickname());
        user.setEmail(defaultUser.getEmail());
        user.setPassword(defaultUser.getPassword());
        user.setId(defaultUser.getId());
        return "mainPage";
    }

    @GetMapping("/register")
    public String getRegisterPage(Model model) {
        return "registerPage";
    }

    @PostMapping("/register")
    public String registerNewUser(@ModelAttribute("newUser") Visitor newUser, Model model) {
        Visitor searchEmailVisitor = userService.getVisitorByEmail(newUser.getEmail());
        Visitor searchNicknameVisitor = userService.getVisitorByNickname(newUser.getNickname());
        if (searchEmailVisitor == null && searchNicknameVisitor == null) {
            newUser.setStatus("Registered");
            newUser = userService.addVisitor(newUser);
            connectionService.sendEmail(newUser.getEmail(), "Registration on MusicOn", "Your email was used to register on MusicOn" +
                    "\nTo confirm registration, please, follow this link:" + "http://localhost:4000/approveRegistration?token=" + cryptService.getAuthToken(newUser.getEmail()));
            return "mainPage";
        } else {
            if (searchEmailVisitor != null) {
                model.addAttribute("errMessage", "Эта электронная почта уже привязана к другому аккаунту");
                return "registerPage";
            } else {
                model.addAttribute("errMessage", "Этот никнейм уже занят");
                return "registerPage";
            }
        }
    }

    @GetMapping("/resetPassword")
    public String getResetPasswordPage(){
        return "resetPasswordPage";
    }

    @PostMapping("/resetPassword")
    public String forgotPassword(@ModelAttribute("newUser") Visitor newUser, Model model){
        Visitor checkVisitor = userService.getVisitorByEmail(newUser.getEmail());
        if (checkVisitor == null) {
            model.addAttribute("errMessage", "Эта электронная почта не зарегистирована");
            return "resetPasswordPage";
        } else {
            String message = userService.isUserValid(checkVisitor);
            if (message.equals("Ok")) {
                String generatedPass = RandomStringUtils.random(8, true, true);
                userService.changeVisitorProperty(checkVisitor.getId(), "password", generatedPass);
                model.addAttribute("infoMessage", "Адрес электронной почты изменён!");
                connectionService.sendEmail(newUser.getEmail(), "Password reset",
                        "Your password was reseted!\nYour new password is: " + generatedPass);
                model.addAttribute("infoMessage", "Password was reseted. Please check your email.");
                return "loginPage";
            } else {
                model.addAttribute("errMessage", message);
                return "resetPasswordPage";
            }

        }
    }

    @GetMapping(value = "/approveRegistration", params = {"token"})
    public String approveRegistration(@RequestParam String token, Model model) {
        Visitor user = userService.getVisitorByEmail(cryptService.decodeToken(token));
        if (user != null) {
            userService.changeVisitorProperty(user.getId(), "status", "User");
            connectionService.sendEmail(user.getEmail(), "Successful Registration", "You've successfully registered on MusicOn!" +
                    "\nYour nickname: " + user.getNickname() + "\nYour password: " + user.getPassword() + "\nEnjoy!");
            model.addAttribute("infoMessage", "Аккаунт подтверждён!");
        }
        model.addAttribute("user", createVisitor());
        return "loginPage";
    }

}
