package com.pokorili.musicOn.controller;

import com.pokorili.musicOn.entity.Advert;
import com.pokorili.musicOn.entity.AdvertPlace;
import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.AdvertService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@SessionAttributes("user")
public class AdvertController {

    private final String UPLOADED_FOLDER = "C:\\temp\\advertisement\\";

    @ModelAttribute("user")
    public Visitor createVisitor() {
        return new Visitor("UnAuth");
    }

    @ModelAttribute("advert")
    public Advert createAdvert(@SessionAttribute("user") Visitor visitor) {
        return new Advert(visitor);
    }

    @Autowired
    AdvertService advertService;

    @GetMapping("/addadvert/{id}")
    public String getAdvertFormPage(@PathVariable Long id, Model model) {
        model.addAttribute("placeId", id);
        return "advertFormPage";
    }

    @PostMapping("/addadvert")
    public String addAdvert(@ModelAttribute("advert") Advert advert, @RequestParam("placeId") Long id, @RequestParam("img") MultipartFile img, @SessionAttribute("user") Visitor visitor) throws IOException {
        byte[] bytes = img.getBytes();
        String generatedString = RandomStringUtils.random(20, true, true);
        Path path = Paths.get(UPLOADED_FOLDER + generatedString + "." + FilenameUtils.getExtension(img.getOriginalFilename()));
        Files.write(path, bytes);
        advert.setPictureLink(String.valueOf(path));
        advert.setVisitor(visitor);
        advert.setAdvertPlace(advertService.getAdvertPlace(id));
        advert = advertService.addAdvert(advert);
        return "mainPage";
    }

    @GetMapping("/adsmanagement")
    public String getManagementPage(Model model) throws IOException {
        Advert[] adverts = advertService.getActiveAdverts();
        AdvertPlace[] advertPlaces = advertService.getFreeAdvertPlaces();
        model.addAttribute("adverts", adverts);
        model.addAttribute("freePlaces", advertPlaces);
        return "advertManagementPage";
    }

    @GetMapping("/deactivateadvert/{advertId}/{advertPlaceId}")
    public String deactivateAdvert(@PathVariable Long advertId, @PathVariable Long advertPlaceId) throws IOException {

        advertService.deleteAdvert(advertId, advertPlaceId);
        return "advertManagementPage";
    }


}
