package com.pokorili.musicOn.controller;

import com.pokorili.musicOn.entity.Playlist;
import com.pokorili.musicOn.entity.Soundtrack;
import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.PlaylistService;
import com.pokorili.musicOn.service.SoundtrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("user")
public class MyMusicController {
    @Autowired
    SoundtrackService soundtrackService;

    @Autowired
    PlaylistService playlistService;


    @ModelAttribute("user")
    public Visitor createVisitor() {
        return new Visitor("UnAuth");
    }

    @GetMapping("/mysoundtracks")
    public String getMySongs(@SessionAttribute("user") Visitor visitor, Model model) throws IOException {
        Soundtrack[] soundtracks = soundtrackService.getSoundtrackByVisitor(visitor.getId());
        model.addAttribute("searchList", soundtracks);
        return "userSoundtracksPage";
    }

    @PostMapping("/mysoundtracks")
    public String getSearchedSongs(@SessionAttribute("user") Visitor visitor, Model model,
                                   @RequestParam(value = "title") String title) throws IOException {
        Soundtrack[] soundtracks = soundtrackService.getSoundtrackByVisitorAndTitle(visitor.getId(), title);
        model.addAttribute("searchList", soundtracks);
        model.addAttribute("title", title);
        return "userSoundtracksPage";
    }

    @GetMapping("/myplaylists")
    public String getMyPlaylists(@SessionAttribute("user") Visitor visitor, Model model) throws IOException {
        Playlist[] playlists = playlistService.getPlaylistsByVisitorId(visitor.getId());
        model.addAttribute("searchList", playlists);
        return "userPlaylistsPage";
    }

    @PostMapping("/myplaylists")
    public String getSearchedPlaylists(@SessionAttribute("user") Visitor visitor, Model model,
                                       @RequestParam(value = "title") String title) throws IOException {
        Playlist[] playlists = playlistService.getPlaylistByVisitorAndTitle(visitor.getId(), title);
        model.addAttribute("searchList", playlists);
        model.addAttribute("title", title);
        return "userPlaylistsPage";
    }

    @GetMapping("/favouriteSoundtracks")
    public String getLikedSoundtracks(@SessionAttribute("user") Visitor user, Model model) throws IOException {
        Soundtrack[] soundtracks = soundtrackService.getLikedSoundtracks(user);
        model.addAttribute("searchList", soundtracks);
        return "userLikedSoundtracksPage";
    }

    @GetMapping("/favouritePlaylists")
    public String getLikedPlaylists(@SessionAttribute("user") Visitor user, Model model) throws IOException {
        Playlist[] playlists = playlistService.getLikedPlaylists(user);
        model.addAttribute("searchList", playlists);
        return "userLikedPlaylistsPage";
    }

}
