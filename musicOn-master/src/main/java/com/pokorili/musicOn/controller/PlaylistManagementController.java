package com.pokorili.musicOn.controller;

import com.pokorili.musicOn.entity.Playlist;
import com.pokorili.musicOn.entity.Soundtrack;
import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.PlaylistService;
import com.pokorili.musicOn.service.SoundtrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
public class PlaylistManagementController {

    @Autowired
    PlaylistService playlistService;

    @Autowired
    SoundtrackService soundtrackService;

    @GetMapping("/deleteplaylist/{id}/{fromPage}")
    public String deletePlaylist(@PathVariable Long id, @PathVariable String fromPage,
                                 @SessionAttribute("user") Visitor visitor, Model model) throws IOException {
        Playlist playlist = playlistService.getPlaylist(id);

        if (playlist.getVisitor().getId() == visitor.getId()) {
            playlistService.deletePlaylist(id);
        }
        Playlist[] playlists = playlistService.getPlaylistsByVisitorId(visitor.getId());
        model.addAttribute("searchList", playlists);
        return fromPage;
    }

    @GetMapping("/deletesoundtrackfromplaylist/{id}/{playlistid}")
    public String deleteSoundtrack(@PathVariable Long id, @PathVariable Long playlistid, Model model) throws IOException {

        playlistService.deleteSoundtrackFromPlaylist(id, playlistid);
        Soundtrack[] soundtracks = soundtrackService.getSoundtracksByPlaylist(playlistid);
        Playlist playlist = playlistService.getPlaylist(playlistid);
        model.addAttribute("playlist", playlist);
        model.addAttribute("soundtracks", soundtracks);
        return "playlistPage";
    }

    @GetMapping("/editplaylist/{id}")
    public String editPlaylist(Model model, @PathVariable Long id){
        if (id == 0){
            model.addAttribute("playlist", new Playlist());
        }
        else{
            model.addAttribute("playlist", playlistService.getPlaylist(id));
        }
        return "editPlaylistPage";
    }

    @PostMapping("/editplaylist")
    public String editPl(Model model, @SessionAttribute("user") Visitor visitor, @ModelAttribute("playlist") Playlist playlist) throws IOException {
        System.out.println(playlist.getId());
        if (playlist.getId() != null){
            Playlist tmp = playlistService.editPlaylist(playlist.getTitle(),
                    playlist.getDescription(), playlist.getId());
            model.addAttribute("playlist", tmp);
            return "playlistPage";
        }
        else{
            playlist.setVisitorId(visitor);
            playlistService.addNewPlaylist(playlist);
            return "userPlaylistsPage";
        }

    }
}
