
package com.pokorili.musicOn.controller;

import com.google.gson.Gson;
import com.pokorili.musicOn.entity.Genre;
import com.pokorili.musicOn.entity.Playlist;
import com.pokorili.musicOn.entity.Soundtrack;
import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.ConnectionService;
import com.pokorili.musicOn.service.PlaylistService;
import com.pokorili.musicOn.service.SoundtrackService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class SoundtrackManagementController {


    @Autowired
    SoundtrackService soundtrackService;

    @Autowired
    ConnectionService connectionService;

    @Autowired
    PlaylistService playlistService;

    @GetMapping("/deletesoundtrack/{id}/{fromPage}/{searchtype}/{title}")
    public String deleteSoundtrack(@PathVariable Long id, @PathVariable String fromPage,
                                   @PathVariable String searchtype, @PathVariable String title, @SessionAttribute("user") Visitor visitor, Model model) throws IOException {
        Soundtrack sd = soundtrackService.getSoundtrackById(id);
        if (sd.getVisitor().getId() == visitor.getId() || visitor.getStatus().equals("Admin")) {
            soundtrackService.deleteSoundtrack(id);
        }

        if (fromPage.equals("searchPage")) {
            model.addAttribute("searchtype", searchtype);
            switch (searchtype){
                case "playlist":
                    Playlist[] playlistList = playlistService.getPlaylists(title);
                    model.addAttribute("searchtype", "playlist");
                    model.addAttribute("searchList", playlistList);
                    model.addAttribute("title", title);
                    break;
                case "soundtrackByTitle":
                    var a = "as";
                    Soundtrack[] soundtrackList = soundtrackService.getSoundtracksBy("title", title);
                    model.addAttribute("searchtype", "soundtrackByTitle");
                    model.addAttribute("title", title);
                    model.addAttribute("searchList", soundtrackList);
                    break;
                case "soundtrackByAuthor":
                    soundtrackList = soundtrackService.getSoundtracksBy("author", title);
                    model.addAttribute("searchtype", "soundtrackByAuthor");
                    model.addAttribute("title", title);
                    model.addAttribute("searchList", soundtrackList);
                    break;
            }
        }
        if (fromPage.equals("genrePage")) {
            System.out.println(searchtype);
            Long genreId = Long.valueOf(searchtype);
            Gson gson = new Gson();
            Genre[] genres = gson.fromJson(connectionService.sendRequest("http://localhost:8080/genres/", "GET",
                    null, null), Genre[].class);
            Soundtrack[] soundtracks = soundtrackService.getSoundtracksByGenreId(genreId);
            model.addAttribute("searchList", soundtracks);
            model.addAttribute("genres", genres);
        }
        if (fromPage.equals("userSoundtracksPage")){
            Soundtrack[] soundtracks = soundtrackService.getSoundtrackByVisitor(visitor.getId());
            model.addAttribute("searchList", soundtracks);
        }
        if (fromPage.equals("userLikedSoundtracksPage")){
            Soundtrack[] soundtracks = soundtrackService.getLikedSoundtracks(visitor);
            model.addAttribute("searchList", soundtracks);
        }
        return fromPage;
    }

    @GetMapping("/getsong/{id}")
    public void getSong(@PathVariable Long id, HttpServletResponse response) throws IOException {
        try {
            InputStream is = new FileInputStream(new File(soundtrackService.getSoundtrackById(id).getLink()));
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
