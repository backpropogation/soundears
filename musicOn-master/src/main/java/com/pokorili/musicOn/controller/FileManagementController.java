package com.pokorili.musicOn.controller;

import com.google.gson.Gson;
import com.pokorili.musicOn.entity.Genre;
import com.pokorili.musicOn.entity.Soundtrack;
import com.pokorili.musicOn.entity.Visitor;
import com.pokorili.musicOn.service.AuthorsAndTitles;
import com.pokorili.musicOn.service.ConnectionService;
import com.pokorili.musicOn.service.SoundtrackService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@Controller
@SessionAttributes("user")
public class FileManagementController {

    private final String UPLOADED_FOLDER = "C://temp//music//";
    private ArrayList<String> paths = new ArrayList<>();

    @Autowired
    SoundtrackService soundtrackService;

    @Autowired
    ConnectionService connectionService;


    @ModelAttribute("user")
    public Visitor createVisitor() {
        return new Visitor("UnAuth");
    }

    @GetMapping("/upload")
    public String getUploadPage() {
        return "uploadPage";
    }

    @PostMapping("/addsongs")
    public String addSongs(@RequestParam("files") MultipartFile[] files, Model model) throws IOException {
        model.addAttribute("files", files);
        Gson gson = new Gson();
        Genre[] genres = gson.fromJson(connectionService.sendRequest("http://localhost:8080/genres/", "GET",
                null, null), Genre[].class);
        model.addAttribute("genres", genres);
        paths = new ArrayList<>();
        for(MultipartFile mf : files){
            byte[] bytes = mf.getBytes();
            String generatedString = RandomStringUtils.random(20, true, true);
            Path path = Paths.get(UPLOADED_FOLDER + generatedString + "." + FilenameUtils.getExtension(mf.getOriginalFilename()));
            Files.write(path, bytes);
            paths.add(String.valueOf(path));
        }
        return "uploadNamePage";
    }

    @PostMapping(value = "/namesongs", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String nameSongs(@RequestBody AuthorsAndTitles au, @SessionAttribute("user") Visitor user, Model model) throws IOException {
        Gson gson = new Gson();
        for (int i =0; i < paths.size(); i++){
            Genre genre = gson.fromJson(connectionService.sendRequest("http://localhost:8080/genres/"+au.getGenreIds()[i], "GET",
                    null, null), Genre.class);
            Soundtrack track = new Soundtrack(user, au.getAuthors()[i], au.getTitles()[i],
                    genre, paths.get(i));
            soundtrackService.addSoundtrack(track);
        }

        model.addAttribute("infoMessage", "Files successfully uploaded");
        return "userSoundtracksPage";
    }


    @GetMapping("/download/{id}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable Long id) throws IOException {

        Soundtrack sd = soundtrackService.getSoundtrackById(id);

        Path path = Paths.get(sd.getLink());
        byte[] data = Files.readAllBytes(path);
        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + path.getFileName().toString())
                .contentType(MediaType.APPLICATION_PDF).contentLength(data.length)
                .body(resource);
    }

}
