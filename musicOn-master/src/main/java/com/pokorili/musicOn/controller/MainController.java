package com.pokorili.musicOn.controller;

import com.google.gson.Gson;
import com.pokorili.musicOn.entity.*;
import com.pokorili.musicOn.service.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@SessionAttributes("user")
public class MainController {

    @Autowired
    SoundtrackService soundtrackService;

    @Autowired
    PlaylistService playlistService;

    @Autowired
    ConnectionService connectionService;

    @Autowired
    AdvertService advertService;

    @ModelAttribute("user")
    public Visitor createVisitor() {
        return new Visitor("UnAuth");
    }

    @GetMapping("/")
    public String getMainPage(Model model) throws IOException {
        Gson gson = new Gson();
        Soundtrack[] soundtracks = gson.fromJson(connectionService.sendRequest("http://localhost:8080/top10songs", "GET", null, null), Soundtrack[].class);
        Playlist[] playlists = gson.fromJson(connectionService.sendRequest("http://localhost:8080/top10playlists", "GET", null, null), Playlist[].class);
        model.addAttribute("soundtracks", soundtracks);
        model.addAttribute("playlists", playlists);
        return "mainPage";
    }

    @CrossOrigin
    @GetMapping(value = "/getadvpic/{id}")
    public void getSong(@PathVariable Long id, HttpServletResponse response){
        try {
            String picLink = advertService.getAdvertById(id).getPictureLink();
            InputStream is = new FileInputStream(new File(picLink));
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/search")
    public String getSearchResults(@RequestParam(value = "title") String title, @RequestParam(value = "searchtype") String searchtype, Model model) throws IOException {

        switch (searchtype){
            case "playlist":
                Playlist[] playlistList = playlistService.getPlaylists(title);
                model.addAttribute("searchtype", "playlist");
                model.addAttribute("searchList", playlistList);
                model.addAttribute("title", title);
                break;
            case "soundtrackByTitle":
                Soundtrack[] soundtrackList = soundtrackService.getSoundtracksBy("title", title);
                model.addAttribute("searchtype", "soundtrackByTitle");
                model.addAttribute("title", title);
                model.addAttribute("searchList", soundtrackList);
                break;
            case "soundtrackByAuthor":
                soundtrackList = soundtrackService.getSoundtracksBy("author", title);
                model.addAttribute("searchtype", "soundtrackByAuthor");
                model.addAttribute("title", title);
                model.addAttribute("searchList", soundtrackList);
                break;
        }
        return "searchPage";
    }

    @GetMapping("/playlist/{id}")
    public String getPlaylist(@PathVariable Long id, Model model) throws IOException {
        Soundtrack[] soundtracks = soundtrackService.getSoundtracksByPlaylist(id);
        Playlist playlist = playlistService.getPlaylist(id);
        model.addAttribute("playlist", playlist);
        model.addAttribute("soundtracks", soundtracks);
        return "playlistPage";
    }

    @GetMapping("/genres")
    public String getGenres(Model model) throws IOException {

        Gson gson = new Gson();
        Genre[] genres = gson.fromJson(connectionService.sendRequest("http://localhost:8080/genres/", "GET",
                null, null), Genre[].class);
        model.addAttribute("genres", genres);
        model.addAttribute("searchList", null);
        return "genrePage";
    }

    @GetMapping("/genres/{id}")
    public String getGenreSongs(@PathVariable Long id, Model model) throws IOException {
        Gson gson = new Gson();
        Genre[] genres = gson.fromJson(connectionService.sendRequest("http://localhost:8080/genres/", "GET",
                null, null), Genre[].class);
        model.addAttribute("genres", genres);
        for (Genre genre : genres){
            if (genre.getId() == id){
                Soundtrack[] soundtracks = soundtrackService.getSoundtracksByGenreId(id);
                model.addAttribute("searchList", soundtracks);
                return "genrePage";
            }
        }
        model.addAttribute("searchList", null);
        return "genrePage";
    }
}
